#include "queuefixed.h"
#include <stdio.h>
#include <iostream>
QueueFixed::QueueFixed(int size)
{
  this->size = size;
  this->sum = 0;
}

int QueueFixed::push(float val)
{
  if(this->vals.size() < this->size)
  {
    // std::cout << "HAVE SPACE" << std::endl;
    this->vals.push_back(val);
    // not full yet. add to sum
    this->sum += val;

  }
  else
  {
    this->sum -= this->vals.at(0);
        this->vals.erase(this->vals.begin());
        this->vals.push_back(val);
    this->sum += val;
  }
}

float QueueFixed::getSum()
{
    return this->sum;
}

int QueueFixed::getSize()
{
    return this->vals.size();
}

QueueFixed::~QueueFixed()
{

}






