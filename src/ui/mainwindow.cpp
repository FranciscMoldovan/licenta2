#include "../include/ui/mainwindow.h"
#include "../build/ui_mainwindow.h"
#include "pidcontroller.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  roiParams.x_rect = 270;
  roiParams.y_rect = 110;
  roiParams.width_rect = 100;
  roiParams.height_rect = 100;

  this->need_pid_update = false;
  this->need_fuzzy_update = false;
  ui->setupUi(this);
  connect(GetUi()->pushButton, SIGNAL(clicked()), this, SLOT(setPIDParameters()));
  connect(GetUi()->btnSetFuzzy, SIGNAL(clicked()), this, SLOT(setFuzzyParameters()));


  GetUi()->btn_up->setAutoRepeat(true);
  GetUi()->btn_down->setAutoRepeat(true);
  GetUi()->btn_left->setAutoRepeat(true);
  GetUi()->btn_right->setAutoRepeat(true);
  GetUi()->btn_vert_incr->setAutoRepeat(true);
  GetUi()->btn_horiz_incr->setAutoRepeat(true);
  GetUi()->btn_vert_decr->setAutoRepeat(true);
  GetUi()->btn_horiz_decr->setAutoRepeat(true);
  connect(GetUi()->btn_up, SIGNAL(pressed()), this, SLOT(moveROIUp()));
  connect(GetUi()->btn_down, SIGNAL(pressed()), this, SLOT(moveROIDown()));
  connect(GetUi()->btn_right, SIGNAL(pressed()), this, SLOT(moveROIRight()));
  connect(GetUi()->btn_left, SIGNAL(pressed()), this, SLOT(moveROILeft()));
  connect(GetUi()->btn_vert_incr, SIGNAL(pressed()), this, SLOT(increaseROIHeight()));
  connect(GetUi()->btn_horiz_incr, SIGNAL(pressed()), this, SLOT(increaseROIWidth()));
  connect(GetUi()->btn_vert_decr, SIGNAL(pressed()), this, SLOT(decreaseROIHeight()));
  connect(GetUi()->btn_horiz_decr, SIGNAL(pressed()), this, SLOT(decreaseROIWidth()));


  GetUi()->progressBarBrightening->setMinimum(0);
  GetUi()->progressBarBrightening->setMaximum(100);

  GetUi()->progressBarDarkening->setMinimum(0);
  GetUi()->progressBarDarkening->setMaximum(100);

  setWindowTitle("Automatic Exposure Application");
}

QImage MainWindow::matToQImage(cv::Mat const &src)
{
  cv::Mat temp; // make the same cv::Mat
  cv::cvtColor(src, temp, CV_BGR2RGB);
  QImage dest((const uchar *)temp.data, temp.cols, temp.rows, temp.step,
              QImage::Format_RGB888);
  dest.bits(); // enforce deep copy
  return dest;
}

void MainWindow::displayImageOnLabel(const QImage &image, QLabel &label)
{
  label.setPixmap(QPixmap::fromImage(image));
  label.show();
}

double MainWindow::getDoubleFromInput(const QLineEdit &textField)
{
  return textField.text().toDouble();
}


Ui::MainWindow *MainWindow::GetUi()
{
  return MainWindow::ui;
}


void MainWindow::setPIDParameters()
{
  std::cout << "\n\nBUTTON PRESSED\n\n"  << std::endl;

  localPIDParams.SamplingTime = this->GetUi()->samplingTimeField->text().toDouble();
  localPIDParams.KP = this->GetUi()->kpField->text().toDouble();
  localPIDParams.KI = this->GetUi()->kiField->text().toDouble();
  localPIDParams.KD = this->GetUi()->kdField->text().toDouble();

  std::cout << "BUTTON PASSES:" << localPIDParams.SamplingTime << "," << localPIDParams.KP << "," << localPIDParams.KI  << "," << localPIDParams.KD
            << std::endl;

  need_pid_update = true;
}

void MainWindow::setFuzzyNeed(bool val)
{
    this->need_fuzzy_update = val;
}

void MainWindow::setFuzzyParameters()
{
  localFuzzyParams.br_vl.clear();
  localFuzzyParams.br_l.clear();
  localFuzzyParams.br_m.clear();
  localFuzzyParams.br_h.clear();
  localFuzzyParams.br_vh.clear();

  localFuzzyParams.lux_vl.clear();
  localFuzzyParams.lux_l.clear();
  localFuzzyParams.lux_m.clear();
  localFuzzyParams.lux_h.clear();
  localFuzzyParams.lux_vh.clear();

  localFuzzyParams.exp_vl.clear();
  localFuzzyParams.exp_l.clear();
  localFuzzyParams.exp_m.clear();
  localFuzzyParams.exp_h.clear();
  localFuzzyParams.exp_vh.clear();



  localFuzzyParams.br_vl.push_back(this->GetUi()->br_vl_1->text().toDouble());
  localFuzzyParams.br_vl.push_back(this->GetUi()->br_vl_2->text().toDouble());
  localFuzzyParams.br_vl.push_back(this->GetUi()->br_vl_3->text().toDouble());
  localFuzzyParams.br_vl.push_back(this->GetUi()->br_vl_4->text().toDouble());
  localFuzzyParams.br_l.push_back(this->GetUi()->br_l_1->text().toDouble());
  localFuzzyParams.br_l.push_back(this->GetUi()->br_l_2->text().toDouble());
  localFuzzyParams.br_l.push_back(this->GetUi()->br_l_3->text().toDouble());
  localFuzzyParams.br_l.push_back(this->GetUi()->br_l_4->text().toDouble());
  localFuzzyParams.br_m.push_back(this->GetUi()->br_m_1->text().toDouble());
  localFuzzyParams.br_m.push_back(this->GetUi()->br_m_2->text().toDouble());
  localFuzzyParams.br_m.push_back(this->GetUi()->br_m_3->text().toDouble());
  localFuzzyParams.br_m.push_back(this->GetUi()->br_m_4->text().toDouble());
  localFuzzyParams.br_h.push_back(this->GetUi()->br_h_1->text().toDouble());
  localFuzzyParams.br_h.push_back(this->GetUi()->br_h_2->text().toDouble());
  localFuzzyParams.br_h.push_back(this->GetUi()->br_h_3->text().toDouble());
  localFuzzyParams.br_h.push_back(this->GetUi()->br_h_4->text().toDouble());
  localFuzzyParams.br_vh.push_back(this->GetUi()->br_vh_1->text().toDouble());
  localFuzzyParams.br_vh.push_back(this->GetUi()->br_vh_2->text().toDouble());
  localFuzzyParams.br_vh.push_back(this->GetUi()->br_vh_3->text().toDouble());
  localFuzzyParams.br_vh.push_back(this->GetUi()->br_vh_4->text().toDouble());


  ///////////////////////////////////////////////////////////////////////////////

  localFuzzyParams.lux_vl.push_back(this->GetUi()->lux_vl_1->text().toDouble());
  localFuzzyParams.lux_vl.push_back(this->GetUi()->lux_vl_2->text().toDouble());
  localFuzzyParams.lux_vl.push_back(this->GetUi()->lux_vl_3->text().toDouble());
  localFuzzyParams.lux_vl.push_back(this->GetUi()->lux_vl_4->text().toDouble());
  localFuzzyParams.lux_l.push_back(this->GetUi() ->lux_l_1->text().toDouble());
  localFuzzyParams.lux_l.push_back(this->GetUi() ->lux_l_2->text().toDouble());
  localFuzzyParams.lux_l.push_back(this->GetUi() ->lux_l_3->text().toDouble());
  localFuzzyParams.lux_l.push_back(this->GetUi() ->lux_l_4->text().toDouble());
  localFuzzyParams.lux_m.push_back(this->GetUi() ->lux_m_1->text().toDouble());
  localFuzzyParams.lux_m.push_back(this->GetUi() ->lux_m_2->text().toDouble());
  localFuzzyParams.lux_m.push_back(this->GetUi() ->lux_m_3->text().toDouble());
  localFuzzyParams.lux_m.push_back(this->GetUi() ->lux_m_4->text().toDouble());
  localFuzzyParams.lux_h.push_back(this->GetUi() ->lux_h_1->text().toDouble());
  localFuzzyParams.lux_h.push_back(this->GetUi() ->lux_h_2->text().toDouble());
  localFuzzyParams.lux_h.push_back(this->GetUi() ->lux_h_3->text().toDouble());
  localFuzzyParams.lux_h.push_back(this->GetUi() ->lux_h_4->text().toDouble());
  localFuzzyParams.lux_vh.push_back(this->GetUi()->lux_vh_1->text().toDouble());
  localFuzzyParams.lux_vh.push_back(this->GetUi()->lux_vh_2->text().toDouble());
  localFuzzyParams.lux_vh.push_back(this->GetUi()->lux_vh_3->text().toDouble());
  localFuzzyParams.lux_vh.push_back(this->GetUi()->lux_vh_4->text().toDouble());

  ////////////////////////////////////////////////////////////////////////////

  localFuzzyParams.exp_vl.push_back(this->GetUi()->exp_vl_1->text().toDouble());
  localFuzzyParams.exp_vl.push_back(this->GetUi()->exp_vl_2->text().toDouble());
  localFuzzyParams.exp_vl.push_back(this->GetUi()->exp_vl_3->text().toDouble());
  localFuzzyParams.exp_vl.push_back(this->GetUi()->exp_vl_4->text().toDouble());
  localFuzzyParams.exp_l.push_back(this->GetUi() ->exp_l_1->text().toDouble());
  localFuzzyParams.exp_l.push_back(this->GetUi() ->exp_l_2->text().toDouble());
  localFuzzyParams.exp_l.push_back(this->GetUi() ->exp_l_3->text().toDouble());
  localFuzzyParams.exp_l.push_back(this->GetUi() ->exp_l_4->text().toDouble());
  localFuzzyParams.exp_m.push_back(this->GetUi() ->exp_m_1->text().toDouble());
  localFuzzyParams.exp_m.push_back(this->GetUi() ->exp_m_2->text().toDouble());
  localFuzzyParams.exp_m.push_back(this->GetUi() ->exp_m_3->text().toDouble());
  localFuzzyParams.exp_m.push_back(this->GetUi() ->exp_m_4->text().toDouble());
  localFuzzyParams.exp_h.push_back(this->GetUi() ->exp_h_1->text().toDouble());
  localFuzzyParams.exp_h.push_back(this->GetUi() ->exp_h_2->text().toDouble());
  localFuzzyParams.exp_h.push_back(this->GetUi() ->exp_h_3->text().toDouble());
  localFuzzyParams.exp_h.push_back(this->GetUi() ->exp_h_4->text().toDouble());
  localFuzzyParams.exp_vh.push_back(this->GetUi()->exp_vh_1->text().toDouble());
  localFuzzyParams.exp_vh.push_back(this->GetUi()->exp_vh_2->text().toDouble());
  localFuzzyParams.exp_vh.push_back(this->GetUi()->exp_vh_3->text().toDouble());
  localFuzzyParams.exp_vh.push_back(this->GetUi()->exp_vh_4->text().toDouble());

  need_fuzzy_update = true;
}

PIDParams MainWindow::getPIDParameters()
{
  return localPIDParams;
}

bool MainWindow:: needPIDUpdate()
{
  if(need_pid_update)
  {
    need_pid_update = false;
    return true;
  }
  return false;
}

bool MainWindow:: needFuzzyUpdate()
{
  if(need_fuzzy_update)
  {
    need_fuzzy_update = false;
    return true;
  }
  return false;
}

FuzzyParams MainWindow::getFuzzyParameters()
{
  return localFuzzyParams;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
  QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Exit Auto Exposure",
                                       tr("Exit Auto Exposure App?\n"),
                                       QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);
  if(resBtn != QMessageBox::Yes)
  {
    event->ignore();
  }
  else
  {
    event->accept();
    exit(0);
  }
}

int MainWindow::moveROIUp()
{
  std:: cout << "UP";
  if(roiParams.y_rect - SZ_ST > 0)
  {
    roiParams.y_rect -= SZ_ST;
  }
}

int MainWindow::moveROIDown()
{
  std:: cout << "DOWN";
  if(roiParams.y_rect + roiParams.height_rect + SZ_ST < 320)
  {
    roiParams.y_rect += SZ_ST;
  }
}

int MainWindow::moveROIRight()
{
  std:: cout << "RIGHT";
  if(roiParams.x_rect + roiParams.width_rect + SZ_ST < 640)
  {
    roiParams.x_rect += SZ_ST;
  }
}

int MainWindow::moveROILeft()
{
  std:: cout << "LEFT";
  if(roiParams.x_rect - SZ_ST > 0)
  {
    roiParams.x_rect -= SZ_ST;
  }
}

int MainWindow::increaseROIHeight()
{
  if(roiParams.y_rect - SZ_ST > 0 && 320 - (roiParams.y_rect + roiParams.height_rect + 2*SZ_ST) > 0)
  {
    roiParams.y_rect -= SZ_ST;
    roiParams.height_rect += 2*SZ_ST;
  }

  else if (roiParams.y_rect - SZ_ST/2 > 0 && 320 - (roiParams.y_rect + roiParams.height_rect + SZ_ST) > 0)
  {
        std :: cout << "ffffffffffffffffffffffffffffff" ;
    roiParams.y_rect -= SZ_ST/2;
    roiParams.height_rect += SZ_ST;
  }
  else if (roiParams.y_rect - SZ_ST/3 > 0 && 320 - (roiParams.y_rect + roiParams.height_rect + 2*SZ_ST/3) > 0)
  {
        std :: cout << "uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu" ;
    roiParams.y_rect -= SZ_ST/3;
    roiParams.height_rect += 2*SZ_ST/3;
  }
  else if (roiParams.y_rect+roiParams.width_rect/2<=160)
      moveROIDown();
  else
      moveROIUp();

}
int MainWindow::increaseROIWidth()
{

  if(roiParams.x_rect - SZ_ST > 0 && (640 - (roiParams.x_rect + roiParams.width_rect + 2*SZ_ST)) > 0)
  {
    roiParams.x_rect -= SZ_ST;
    roiParams.width_rect += 2*SZ_ST;
  }
  else if (roiParams.x_rect - SZ_ST/2 > 0 && (640 - (roiParams.x_rect + roiParams.width_rect + SZ_ST)) > 0)
  {
      roiParams.x_rect -= SZ_ST/2;
      roiParams.width_rect += SZ_ST;
  }
  else if (roiParams.x_rect - SZ_ST/3 > 0 && (640 - (roiParams.x_rect + roiParams.width_rect + 2*SZ_ST/3)) > 0)
  {
      roiParams.x_rect -= SZ_ST/3;
      roiParams.width_rect += 2*SZ_ST/3;
  }
  else if (roiParams.x_rect+roiParams.width_rect/2<=320)
      moveROIRight();
  else
      moveROILeft();
}

int MainWindow::decreaseROIHeight()
{
  if(roiParams.height_rect>2*SZ_ST)
  {
    roiParams.y_rect += SZ_ST;
    roiParams.height_rect -= 2*SZ_ST;
  }
}

int MainWindow::decreaseROIWidth()
{
  if(roiParams.width_rect>2*SZ_ST)
  {
    roiParams.x_rect += SZ_ST;
    roiParams.width_rect -= 2*SZ_ST;
  }
}

int MainWindow::currentTab()
{
  return GetUi()->tabWidget->currentIndex();
}

MainWindow::~MainWindow()
{
  delete ui;
}





