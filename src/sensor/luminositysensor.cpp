#include "luminositysensor.h"

class LSensorImpl
{
public:
  LSensorImpl();
  uint32_t MeasureLuminosity();
  ~LSensorImpl();
private:
  int rc;
  uint16_t broadband, ir;
  uint32_t lux ;
  TSL2561 light1;
};

LuminositySensor::LuminositySensor()
{
  limpl = new LSensorImpl();
}

uint32_t LuminositySensor::MeasureLuminosity()
{
    return limpl->MeasureLuminosity();
}

LuminositySensor::~LuminositySensor()
{
  delete limpl;
}

LSensorImpl::LSensorImpl(): lux {0}
{
  // prepare the sensor
  // param1: raspberry pi i2c master controller attached
  //         to the TSL2561
  // param2: i2c selection jumper ( for the used sensor
  //         the address is 0x39
  light1 = TSL2561_INIT(1, TSL2561_ADDR_FLOAT);

  // initialize the sensor
  rc = TSL2561_OPEN(&light1);
  if(rc != 0)
  {
    fprintf(stderr, "Error initializing TSL2561 sensor (%s). Check \
                        your i2c bus(ex: i2cdetect)\n", strerror(light1.lasterr));
    TSL2561_CLOSE(&light1);
  }

  // set the gain to 1X (it can be TSL2561_GAIN_1X or TSL2561_GAIN16X)
  // use auto gain to get more precission in dark ambients, or enable auto
  // gain below
  rc = TSL2561_SETGAIN(&light1, TSL2561_GAIN_1X);


  // set the integration time
  // (TSL2561_INTEGRATIONTIME_402MS or TSL2561_INTEGRATIONTIME_101MS or TSL2561_INTEGRATIONTIME_13MS)
  // TSL2561_INTEGRATIONTIME_402MS is slower but more precise, TSL2561_INTEGRATIONTIME_13MS is very fast but not so precise

  // I choose 13MS, which gives a theoretical 76 samples/second, essential for 30fps video
  rc = TSL2561_SETINTEGRATIONTIME(&light1, TSL2561_INTEGRATIONTIME_13MS);
}

/**
 * @brief LSensorImpl::MeasureLuminosity
 * - senses the luminosity from the sensor (lux is
 * the luminosity taken in "lux measure units")
 *      -the last parameter can be 1 to enable library auto gain,
 *       or 0 to disable it
 * @return measured luminosity
 */
uint32_t LSensorImpl::MeasureLuminosity()
{
    rc = TSL2561_SENSELIGHT(&light1, &broadband, &ir, &lux, 0);
    return lux;
}

LSensorImpl::~LSensorImpl()
{
  TSL2561_CLOSE(&light1);
}
