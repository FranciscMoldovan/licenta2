#include <raspicam/raspicam.h>
#include <raspicam/raspicam_cv.h>
#include <iostream>

#include "pidcontroller.h"
#include "luminositysensor.h"
#define SIZE 20

#include <FuzzyRule.h>
#include <FuzzyComposition.h>
#include <Fuzzy.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>
#include <FuzzyIO.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>

#define BRIGHTNESS_MIN 0
#define BRIGHTNESS_MAX 100

// value of exposure in us
#define EXPOSURE_MIN 1
#define EXPOSURE_MAX 100000

//#include "main.h"
#include "exp_algs.h"

extern raspicam::RaspiCam_Cv Camera; // Camera Object
extern PID pid;
extern Fuzzy *fuzzy;
extern cv::Mat roiImg;
extern cv::Rect rect;
extern cv::Scalar textColor;
extern int brightness;
extern LuminositySensor LightSensor;
extern double exposure;
extern int lux_measured;

extern FuzzySet *exposure__VL;
extern FuzzySet *exposure__L ;
extern FuzzySet *exposure__M ;
extern FuzzySet *exposure__H ;
extern FuzzySet *exposure__VH;

extern FuzzySet *br__VL;
extern FuzzySet *br__L ;
extern FuzzySet *br__M ;
extern FuzzySet *br__H ;
extern FuzzySet *br__VH;

extern FuzzySet *light__VL;
extern FuzzySet *light__L ;
extern FuzzySet *light__M ;
extern FuzzySet *light__H ;
extern FuzzySet *light__VH;

double map(double x, double in_min, double in_max, double out_min, double out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void auto_exposure1()
{
  std::cout << "BASIC!" << std::endl;
  int increment = Camera.get(CV_CAP_PROP_EXPOSURE) / 6;
  if(brightness < 60)
  {
    exposure = Camera.get(CV_CAP_PROP_EXPOSURE) + increment;
    Camera.set(CV_CAP_PROP_EXPOSURE, exposure);
  }
  else if((int)Camera.get(CV_CAP_PROP_EXPOSURE) + 10 > 0)
  {
    exposure = Camera.get(CV_CAP_PROP_EXPOSURE) + 10;
    Camera.set(CV_CAP_PROP_EXPOSURE, exposure);
  }

  if(brightness > 40)
    if((int)Camera.get(CV_CAP_PROP_EXPOSURE) - increment > 0)
    {
      exposure = Camera.get(CV_CAP_PROP_EXPOSURE) - increment;
      Camera.set(CV_CAP_PROP_EXPOSURE, exposure);
    }
    else if((int)Camera.get(CV_CAP_PROP_EXPOSURE) - 10 > 0)
    {
      exposure = Camera.get(CV_CAP_PROP_EXPOSURE) - 10;
      Camera.set(CV_CAP_PROP_EXPOSURE, exposure);
    }
}
void auto_exposure2()
{
  std::cout << "PID!" << std::endl;
  // PID control for exposure
  double exp = pid.calculate(BRIGHTNESS_MAX / 2, brightness);
  //  std::cout << "PID CALCULATION=" << exp << " ";
  exposure = map(exp, BRIGHTNESS_MIN, BRIGHTNESS_MAX, EXPOSURE_MIN, EXPOSURE_MAX);

  Camera.set(CV_CAP_PROP_EXPOSURE, exposure);
}

int estimate_setsize();
void auto_exposure3()
{
  //  std::cout << "FUZZY!" << std::endl;
  // Step 1: Instantiating a fuzzy object
  fuzzy = new Fuzzy();

  // The image brightness measured from current frame
  FuzzyInput *crtImgBrght = new FuzzyInput(1);

  // The exposure given to the camera
  FuzzyOutput *crtExposure = new FuzzyOutput(1);

  static double magic_number = estimate_setsize();


  std::cout << "MAGIC NUMBER ISSSSS: " << magic_number << std::endl;
  //  std::cout << " >>> the brightness=" << brightness << "\n";
  if(brightness < 35)
  {
    if(magic_number < 1024)
    {
      magic_number *= 2;
    }
    //    std::cout  << "STILL SMALLER" << std::endl;
  }
  if(brightness > 65)
  {
    if(magic_number > 0.25)
    {
      magic_number /= 2;
    }
  }

  exposure__VL = new FuzzySet(1 * magic_number  , 1 * magic_number    , 10 * magic_number , 30 * magic_number);
  exposure__L  = new FuzzySet(10 * magic_number , 30 * magic_number, 30 * magic_number , 50 * magic_number);
  exposure__M  = new FuzzySet(30 * magic_number, 50 * magic_number, 50 * magic_number, 70 * magic_number);
  exposure__H  = new FuzzySet(50 * magic_number, 70 * magic_number, 70 * magic_number, 90 * magic_number);
  exposure__VH = new FuzzySet(70 * magic_number, 90 * magic_number, 100 * magic_number, 100 * magic_number);

  crtExposure->addFuzzySet(exposure__VL);
  crtExposure->addFuzzySet(exposure__L);
  crtExposure->addFuzzySet(exposure__M);
  crtExposure->addFuzzySet(exposure__H);
  crtExposure->addFuzzySet(exposure__VH);

  fuzzy->addFuzzyOutput(crtExposure);



  crtImgBrght->addFuzzySet(br__VL);
  crtImgBrght->addFuzzySet(br__L);
  crtImgBrght->addFuzzySet(br__M);
  crtImgBrght->addFuzzySet(br__H);
  crtImgBrght->addFuzzySet(br__VH);

  fuzzy->addFuzzyInput(crtImgBrght);

  FuzzyRuleConsequent *thenExpVL = new FuzzyRuleConsequent();
  FuzzyRuleConsequent *thenExpL  = new FuzzyRuleConsequent();
  FuzzyRuleConsequent *thenExpM  = new FuzzyRuleConsequent();
  FuzzyRuleConsequent *thenExpH  = new FuzzyRuleConsequent();
  FuzzyRuleConsequent *thenExpVH = new FuzzyRuleConsequent();

  thenExpVL->addOutput(exposure__VL);
  thenExpL ->addOutput(exposure__L);
  thenExpM ->addOutput(exposure__M);
  thenExpH ->addOutput(exposure__H);
  thenExpVH->addOutput(exposure__VH);


  FuzzyRuleAntecedent *if_brightnessVL = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent *if_brightnessL  = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent *if_brightnessM  = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent *if_brightnessH  = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent *if_brightnessVH = new FuzzyRuleAntecedent();


  if_brightnessVL -> joinSingle(br__VL);
  if_brightnessL  -> joinSingle(br__L);
  if_brightnessM  -> joinSingle(br__M);
  if_brightnessH  -> joinSingle(br__H);
  if_brightnessVH -> joinSingle(br__VH);



  FuzzyRule *fuzzyRule01 = new FuzzyRule(1, if_brightnessVL, thenExpVH);
  FuzzyRule *fuzzyRule02 = new FuzzyRule(2, if_brightnessL , thenExpH);
  FuzzyRule *fuzzyRule03 = new FuzzyRule(3, if_brightnessM , thenExpM);
  FuzzyRule *fuzzyRule04 = new FuzzyRule(4, if_brightnessH , thenExpL);
  FuzzyRule *fuzzyRule05 = new FuzzyRule(5, if_brightnessVH, thenExpVL);


  fuzzy->addFuzzyRule(fuzzyRule01);
  fuzzy->addFuzzyRule(fuzzyRule02);
  fuzzy->addFuzzyRule(fuzzyRule03);
  fuzzy->addFuzzyRule(fuzzyRule04);
  fuzzy->addFuzzyRule(fuzzyRule05);


  fuzzy->setInput(1, (float)brightness);
  fuzzy->fuzzify();
  float exp_fuzzy = 0;
  exposure = fuzzy->defuzzify(1);

  //  std::cout << "DEFUZZIFIED:" << std::cout << exp_fuzzy << std::endl;

  Camera.set(CV_CAP_PROP_EXPOSURE, exposure);
}

int estimate_setsize()
{
  if(lux_measured > 0)
  {
    if(lux_measured < 100)
    {
      return 32;
    }
    else if(lux_measured < 500)
    {
      return 16;
    }
    else if(lux_measured < 1000)
    {
      return 8;
    }
    else
    {
      return 2;
    }
  }
}


void fuzzy_init()
{
  std::cout << "FUZZY!" << std::endl;
  // Step 1: Instantiating a fuzzy object
  fuzzy = new Fuzzy();

  // The image brightness measured from current frame
  FuzzyInput *crtImgBrght = new FuzzyInput(1);

  // The exposure given to the camera
  FuzzyOutput *crtExposure = new FuzzyOutput(1);

  // Creating the exposure fuzzy sets
  //  exposure__VL = new FuzzySet(1    , 1    , 3300 , 9900);
  //  exposure__L  = new FuzzySet(3300 , 9900 , 9900 , 16500);
  //  exposure__M  = new FuzzySet(9900 , 16500, 16500, 23100);
  //  exposure__H  = new FuzzySet(16500, 23100, 23100, 29700);
  //  exposure__VH = new FuzzySet(23100, 29700, 33000, 33000);

  static double magic_number = 1;



  crtExposure->addFuzzySet(exposure__VL);
  crtExposure->addFuzzySet(exposure__L);
  crtExposure->addFuzzySet(exposure__M);
  crtExposure->addFuzzySet(exposure__H);
  crtExposure->addFuzzySet(exposure__VH);

  fuzzy->addFuzzyOutput(crtExposure);



  // Creating the ambient light fuzzy sets
  //  light__VL = new FuzzySet(0, 0, 10, 60);
  //  light__L  = new FuzzySet(10, 60, 60, 450);
  //  light__M  = new FuzzySet(60, 450, 450, 1400);
  //  light__H  = new FuzzySet(450, 1400, 1400, 6000);
  //  light__VH = new FuzzySet(1400, 6000, 10000, 10000);


  // Creating brightness fuzzy sets
  //  br__VL = new FuzzySet(0 , 0 , 10, 30);
  //  br__L  = new FuzzySet(10, 30, 30, 50);
  //  br__M  = new FuzzySet(30, 50, 50, 70);
  //  br__H  = new FuzzySet(50, 70, 70, 90);
  //  br__VH = new FuzzySet(70, 90, 90, 100);

  std::cout << "BRIGHTNESS VERY LOW\n";

  std::cout << "A:";
  std::cout << br__VL->getPointA();
  std::cout << "B:";
  std::cout << br__VL->getPointB();
  std::cout << "C:";
  std::cout << br__VL->getPointC();
  std::cout << "D:";
  std::cout << br__VL->getPointD();

  crtImgBrght->addFuzzySet(br__VL);
  crtImgBrght->addFuzzySet(br__L);
  crtImgBrght->addFuzzySet(br__M);
  crtImgBrght->addFuzzySet(br__H);
  crtImgBrght->addFuzzySet(br__VH);

  fuzzy->addFuzzyInput(crtImgBrght);

  FuzzyRuleConsequent *thenExpVL = new FuzzyRuleConsequent();
  FuzzyRuleConsequent *thenExpL  = new FuzzyRuleConsequent();
  FuzzyRuleConsequent *thenExpM  = new FuzzyRuleConsequent();
  FuzzyRuleConsequent *thenExpH  = new FuzzyRuleConsequent();
  FuzzyRuleConsequent *thenExpVH = new FuzzyRuleConsequent();

  thenExpVL->addOutput(exposure__VL);
  thenExpL ->addOutput(exposure__L);
  thenExpM ->addOutput(exposure__M);
  thenExpH ->addOutput(exposure__H);
  thenExpVH->addOutput(exposure__VH);


  FuzzyRuleAntecedent *if_brightnessVL = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent *if_brightnessL  = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent *if_brightnessM  = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent *if_brightnessH  = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent *if_brightnessVH = new FuzzyRuleAntecedent();


  if_brightnessVL -> joinSingle(br__VL);
  if_brightnessL  -> joinSingle(br__L);
  if_brightnessM  -> joinSingle(br__M);
  if_brightnessH  -> joinSingle(br__H);
  if_brightnessVH -> joinSingle(br__VH);



  FuzzyRule *fuzzyRule01 = new FuzzyRule(1, if_brightnessVL, thenExpVH);
  FuzzyRule *fuzzyRule02 = new FuzzyRule(2, if_brightnessL , thenExpH);
  FuzzyRule *fuzzyRule03 = new FuzzyRule(3, if_brightnessM , thenExpM);
  FuzzyRule *fuzzyRule04 = new FuzzyRule(4, if_brightnessH , thenExpL);
  FuzzyRule *fuzzyRule05 = new FuzzyRule(5, if_brightnessVH, thenExpVL);


  fuzzy->addFuzzyRule(fuzzyRule01);
  fuzzy->addFuzzyRule(fuzzyRule02);
  fuzzy->addFuzzyRule(fuzzyRule03);
  fuzzy->addFuzzyRule(fuzzyRule04);
  fuzzy->addFuzzyRule(fuzzyRule05);


  //  fuzzy->setInput(1, (float)brightness);
  //  fuzzy->fuzzify();
  //  float exp_fuzzy = 0;
  //  exposure = fuzzy->defuzzify(1);

  ////  std::cout << "DEFUZZIFIED:" << std::cout << exp_fuzzy << std::endl;

  //  Camera.set(CV_CAP_PROP_EXPOSURE, exposure);

}

void auto_exposure4()
{

}



