#ifndef _PID_SOURCE_
#define _PID_SOURCE_

#include <iostream>
#include <cmath>
#include "pidcontroller.h"
#include <chrono>

using namespace std;
class PIDImpl
{
public:
  PIDImpl(double dt, double max, double min, double Kp, double Kd, double Ki);
  ~PIDImpl();
  double calculate(double setpoint, double pv);
  void setDT(double dt); // sets DT in ms
  double getDT(); // returns DT in ms
  double getP();
  double getI();
  double getD();

  void setP(double kp);
  void setI(double ki);
  void setD(double kd);
private:
  double _dt;
  double _max;
  double _min;
  double _Kp;
  double _Kd;
  double _Ki;
  double _pre_error;
  double _integral;
};


PID::PID(double dt, double max, double min, double Kp, double Kd, double Ki)
{
  pimpl = new PIDImpl(dt, max, min, Kp, Kd, Ki);
}
PID::PID()
{
  pimpl = new PIDImpl(33, 100, 0, 0, 0, 0);
}

double PID::calculate(double setpoint, double pv)
{
  return pimpl->calculate(setpoint, pv);
}

void PID::setDT(double dt)
{
  pimpl->setDT(dt);
}

// PID class getters
double PID::getDT()
{
  return pimpl->getDT();
}

double PID::getP()
{
  return pimpl->getP();
}

double PID::getI()
{
  return pimpl->getI();
}

double PID::getD()
{
  return pimpl->getD();
}

PID::~PID()
{
  delete pimpl;
}


/**
 * Implementation
 */
PIDImpl::PIDImpl(double dt, double max, double min, double Kp, double Kd, double Ki) :
  _dt(dt),
  _max(max),
  _min(min),
  _Kp(Kp),
  _Kd(Kd),
  _Ki(Ki),
  _pre_error(0),
  _integral(0)
{
}

double PIDImpl::getDT()
{
  return this->_dt;
}

void PIDImpl::setDT(double dt)
{
  this->_dt = dt;
}

// PIDImpl class controller paraterers getters
double PIDImpl::getP()
{
  return this->_Kp;
}

double PIDImpl::getI()
{
  return this->_Ki;
}

double PIDImpl::getD()
{
  return this->_Kd;
}

// PidImpl class PID parameters setters
void PIDImpl::setP(double kp)
{
  this ->_Kp = kp;
}

void PIDImpl::setI(double ki)
{
  this->_Ki = ki;
}

void PIDImpl::setD(double kd)
{
  this->_Kd = kd;
}


// PID class PID parameters setters
void PID::setP(double kp)
{
  pimpl->setP(kp);
}

void PID::setI(double ki)
{
  pimpl->setI(ki);
}

void PID::setD(double kd)
{
  pimpl->setD(kd);
}


using namespace std::chrono;
auto last = high_resolution_clock::now();
auto last_output = 0;
double PIDImpl::calculate(double setpoint, double pv)
{
  auto now   = high_resolution_clock::now();
  auto mseconds = duration_cast<milliseconds>(now - last).count();
  if(mseconds > _dt)
  {
    std::cout << "\n\n[PIDebug]\n\n" << std::endl;
    last = now;
    // Calculate error
    double error = setpoint - pv;

    // Proportional term
    double Pout = _Kp * error;

    // Integral term
    _integral += error * _dt;
    double Iout = _Ki * _integral;

    // Derivative term
    double derivative = (error - _pre_error) / _dt;
    double Dout = _Kd * derivative;

    // Calculate total output
    double output = Pout + Iout + Dout;

    // Restrict to max/min
    if(output > _max)
    {
      output = _max;
    }
    else if(output < _min)
    {
      output = _min;
    }

    // Save error to previous error
    _pre_error = error;
    last_output = output;
    return output;
  }
  return last_output;
}

PIDImpl::~PIDImpl()
{
}

#endif
