#include <rapidxml.hpp>
#include <rapidxml_utils.hpp>
#include <rapidxml_print.hpp>
//#include <main.h>
#include <exp_algs.h>
#include <iostream>
#include <sstream>
#include <fstream>

#include <raspicam/raspicam.h>
#include <raspicam/raspicam_cv.h>
#include <iostream>

#include "pidcontroller.h"
#include "luminositysensor.h"
#define SIZE 20

#include <FuzzyRule.h>
#include <FuzzyComposition.h>
#include <Fuzzy.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>
#include <FuzzyIO.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>

#include <read_xml.h>
#include <pidcontroller.h>


extern raspicam::RaspiCam_Cv Camera; // Camera Object
extern PID pid;
extern Fuzzy *fuzzy;
extern cv::Mat roiImg;
extern cv::Rect rect;
extern cv::Scalar textColor;
extern int brightness;
extern LuminositySensor LightSensor;
extern double exposure;


extern FuzzySet *exposure__VL;
extern FuzzySet *exposure__L ;
extern FuzzySet *exposure__M ;
extern FuzzySet *exposure__H ;
extern FuzzySet *exposure__VH;

extern FuzzySet *br__VL;
extern FuzzySet *br__L ;
extern FuzzySet *br__M ;
extern FuzzySet *br__H ;
extern FuzzySet *br__VH;

extern FuzzySet *light__VL;
extern FuzzySet *light__L ;
extern FuzzySet *light__M ;
extern FuzzySet *light__H ;
extern FuzzySet *light__VH;


using namespace rapidxml;
using namespace std;

void read_xml_params()
{
  xml_document<> doc;
  file<> xmlFile("../data/config.xml");
  doc.parse<0>(xmlFile.data());

  // nodes for each fuzzy set's four values
  xml_node<> *A, *B, *C, *D;

  xml_node<> *root = doc.first_node("root");
  xml_node<> *node_pid = root->first_node("pid");
  xml_node<> *node_dt = node_pid->first_node("dt");
  xml_node<> *node_kp = node_pid->first_node("kp");
  xml_node<> *node_ki = node_pid->first_node("ki");
  xml_node<> *node_kd = node_pid->first_node("kd");

  cout << "dt=" << node_dt->value() << endl;
  cout << "P=" << node_kp->value() << endl;
  cout << "I=" << node_ki->value() << endl;
  cout << "D=" << node_kd->value() << endl;

  pid.setDT(std::atof(node_dt->value())) ;
  pid.setP(std::atof(node_kp->value())) ;
  pid.setI(std::atof(node_ki->value())) ;
  pid.setD(std::atof(node_kd->value())) ;


  xml_node<> *node_fuzzy = root->first_node("fuzzy");
  xml_node<> *node_fuzzy_IO = node_fuzzy->first_node("brightness"); // brightness
  xml_node<> *node_fuzzy_IO_set;

  node_fuzzy_IO_set = node_fuzzy_IO->first_node("VL"); // br__vl
  A = node_fuzzy_IO_set->first_node();
  B = A->next_sibling();
  C = B->next_sibling();
  D = C->next_sibling();

  br__VL = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                        std::atof(C->value()), std::atof(D->value()));


  node_fuzzy_IO_set = node_fuzzy_IO->first_node("L"); // br__l
  A = node_fuzzy_IO_set->first_node();
  B = A->next_sibling();
  C = B->next_sibling();
  D = C->next_sibling();

  br__L = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                        std::atof(C->value()), std::atof(D->value()));


  node_fuzzy_IO_set = node_fuzzy_IO->first_node("M"); // br__m
  A = node_fuzzy_IO_set->first_node();
  B = A->next_sibling();
  C = B->next_sibling();
  D = C->next_sibling();

  br__M = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                        std::atof(C->value()), std::atof(D->value()));

  node_fuzzy_IO_set = node_fuzzy_IO->first_node("H"); // br__h
  A = node_fuzzy_IO_set->first_node();
  B = A->next_sibling();
  C = B->next_sibling();
  D = C->next_sibling();

  br__H = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                        std::atof(C->value()), std::atof(D->value()));

  node_fuzzy_IO_set = node_fuzzy_IO->first_node("VH"); // br__vh
  A = node_fuzzy_IO_set->first_node();
  B = A->next_sibling();
  C = B->next_sibling();
  D = C->next_sibling();

  br__VH = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                        std::atof(C->value()), std::atof(D->value()));

//// ///___///___///___///___///___///___///___///___///___///___///___///___
 node_fuzzy_IO = node_fuzzy->first_node("lux");  // lux

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("VL"); // lux__vl
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 light__VL = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("L"); // lux__l
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 light__L = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("M"); // lux__m
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 light__M = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("H"); // lux__h
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 light__H = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("VH"); // lux__vh
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 light__VH = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));


//// ///___///___///___///___///___///___///___///___///___///___///___///___
 node_fuzzy_IO = node_fuzzy->first_node("exposure");  // exposure

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("VL"); // exposure__VL
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 exposure__VL = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("L"); // exposure__L
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 exposure__L = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("M"); // exposure__M
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 exposure__M = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("H"); // exposure__H
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 exposure__H = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));

 node_fuzzy_IO_set = node_fuzzy_IO->first_node("VH"); // exposure__VH
 A = node_fuzzy_IO_set->first_node();
 B = A->next_sibling();
 C = B->next_sibling();
 D = C->next_sibling();

 exposure__VH = new FuzzySet(std::atof(A->value()), std::atof(B->value()),
                       std::atof(C->value()), std::atof(D->value()));

}

#include <iomanip>
template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 6)
{
    std::ostringstream out;
    out << std::setprecision(n) << a_value;
    return out.str();
}

void persist_data(PIDParams pidParams)
{
    xml_document<> doc;
    file<> xmlFile("../data/config.xml");
    doc.parse<parse_declaration_node|parse_no_data_nodes>(xmlFile.data());

    xml_node<> *root = doc.first_node("root");
    xml_node<> *node_pid = root->first_node("pid");
    xml_node<> *node_dt = node_pid->first_node("dt");
    xml_node<> *node_kp = node_pid->first_node("kp");
    xml_node<> *node_ki = node_pid->first_node("ki");
    xml_node<> *node_kd = node_pid->first_node("kd");

    std::string dt_str=to_string_with_precision(pidParams.SamplingTime);
    node_dt->value(dt_str.c_str());

    std::string kp_str=to_string_with_precision(pidParams.KP);
    node_kp->value(kp_str.c_str());

    std::string ki_str=to_string_with_precision(pidParams.KI);
    node_ki->value(ki_str.c_str());

    std::string kd_str=to_string_with_precision(pidParams.KD);
    node_kd->value(kd_str.c_str());

    std::ofstream file_stored("../data/config.xml");
    file_stored  << doc;
    file_stored.close();
}

void persist_data(FuzzyParams fuzzyParams)
{
    xml_document<> doc;
    file<> xmlFile("../data/config.xml");
    doc.parse<parse_declaration_node|parse_no_data_nodes>(xmlFile.data());

    // nodes for each fuzzy set's four values
    xml_node<> *A, *B, *C, *D;

    xml_node<> *root = doc.first_node("root");

    xml_node<> *node_fuzzy = root->first_node("fuzzy");
    xml_node<> *node_fuzzy_IO = node_fuzzy->first_node("brightness"); // brightness
    xml_node<> *node_fuzzy_IO_set;



    node_fuzzy_IO_set = node_fuzzy_IO->first_node("VL"); // br__vl
    A = node_fuzzy_IO_set->first_node();
    B = A->next_sibling();
    C = B->next_sibling();
    D = C->next_sibling();

    std::string a=to_string_with_precision(fuzzyParams.br_vl.at(0), 6);
    A->value(a.c_str());
    std::string aa=to_string_with_precision(fuzzyParams.br_vl.at(1), 6);
    B->value(aa.c_str());
    std::string aaa=to_string_with_precision(fuzzyParams.br_vl.at(2), 6);
    C->value(aaa.c_str());
    std::string aaaa=to_string_with_precision(fuzzyParams.br_vl.at(3), 6);
    D->value(aaaa.c_str());


    node_fuzzy_IO_set = node_fuzzy_IO->first_node("L"); // br__l
    A = node_fuzzy_IO_set->first_node();
    B = A->next_sibling();
    C = B->next_sibling();
    D = C->next_sibling();

    std::string br_l0 = to_string_with_precision(fuzzyParams.br_l.at(0), 6);
    A->value(br_l0.c_str());
    std::string br_l1 = to_string_with_precision(fuzzyParams.br_l.at(1), 6);
    B->value(br_l1.c_str());
    std::string br_l2 = to_string_with_precision(fuzzyParams.br_l.at(2), 6);
    C->value(br_l2.c_str());
    std::string br_l3 = to_string_with_precision(fuzzyParams.br_l.at(3), 6);
    D->value(br_l3.c_str());


    node_fuzzy_IO_set = node_fuzzy_IO->first_node("M"); // br__m
    A = node_fuzzy_IO_set->first_node();
    B = A->next_sibling();
    C = B->next_sibling();
    D = C->next_sibling();

    std::string br_m0 = to_string_with_precision(fuzzyParams.br_m.at(0), 6);
    A->value(br_m0.c_str());
    std::string br_m1 = to_string_with_precision(fuzzyParams.br_m.at(1), 6);
    B->value(br_m1.c_str());
    std::string br_m2 = to_string_with_precision(fuzzyParams.br_m.at(2), 6);
    C->value(br_m2.c_str());
    std::string br_m3 = to_string_with_precision(fuzzyParams.br_m.at(3), 6);
    D->value(br_m3.c_str());


    node_fuzzy_IO_set = node_fuzzy_IO->first_node("H"); // br__h
    A = node_fuzzy_IO_set->first_node();
    B = A->next_sibling();
    C = B->next_sibling();
    D = C->next_sibling();

    std::string br_h0 = to_string_with_precision(fuzzyParams.br_h.at(0), 6);
    A->value(br_h0.c_str());
    std::string br_h1 = to_string_with_precision(fuzzyParams.br_h.at(1), 6);
    B->value(br_h1.c_str());
    std::string br_h2 = to_string_with_precision(fuzzyParams.br_h.at(2), 6);
    C->value(br_h2.c_str());
    std::string br_h3 = to_string_with_precision(fuzzyParams.br_h.at(3), 6);
    D->value(br_h3.c_str());


    node_fuzzy_IO_set = node_fuzzy_IO->first_node("VH"); // br__vh
    A = node_fuzzy_IO_set->first_node();
    B = A->next_sibling();
    C = B->next_sibling();
    D = C->next_sibling();

    std::string br_vh0 = to_string_with_precision(fuzzyParams.br_vh.at(0), 6);
    A->value(br_vh0.c_str());
    std::string br_vh1 = to_string_with_precision(fuzzyParams.br_vh.at(1), 6);
    B->value(br_vh1.c_str());
    std::string br_vh2 = to_string_with_precision(fuzzyParams.br_vh.at(2), 6);
    C->value(br_vh2.c_str());
    std::string br_vh3 = to_string_with_precision(fuzzyParams.br_vh.at(3), 6);
    D->value(br_vh3.c_str());


//    //// ///___///___///___///___///___///___///___///___///___///___///___///___
     node_fuzzy_IO = node_fuzzy->first_node("lux");  // lux
     node_fuzzy_IO_set = node_fuzzy_IO->first_node("VL"); // lux__vl
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string lux_vl0 = to_string_with_precision(fuzzyParams.lux_vl.at(0), 6);
     A->value(lux_vl0.c_str());
     std::string lux_vl1 = to_string_with_precision(fuzzyParams.lux_vl.at(1), 6);
     B->value(lux_vl1.c_str());
     std::string lux_vl2 = to_string_with_precision(fuzzyParams.lux_vl.at(2), 6);
     C->value(lux_vl2.c_str());
     std::string lux_vl3 = to_string_with_precision(fuzzyParams.lux_vl.at(3), 6);
     D->value(lux_vl3.c_str());




     node_fuzzy_IO_set = node_fuzzy_IO->first_node("L"); // lux__l
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string lux_l0 = to_string_with_precision(fuzzyParams.lux_l.at(0), 6);
     A->value(lux_l0.c_str());
     std::string lux_l1 = to_string_with_precision(fuzzyParams.lux_l.at(1), 6);
     B->value(lux_l1.c_str());
     std::string lux_l2 = to_string_with_precision(fuzzyParams.lux_l.at(2), 6);
     C->value(lux_l2.c_str());
     std::string lux_l3 = to_string_with_precision(fuzzyParams.lux_l.at(3), 6);
     D->value(lux_l3.c_str());



     node_fuzzy_IO_set = node_fuzzy_IO->first_node("M"); // lux__m
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string lux_m0 = to_string_with_precision(fuzzyParams.lux_m.at(0), 6);
     A->value(lux_m0.c_str());
     std::string lux_m1 = to_string_with_precision(fuzzyParams.lux_m.at(1), 6);
     B->value(lux_m1.c_str());
     std::string lux_m2 = to_string_with_precision(fuzzyParams.lux_m.at(2), 6);
     C->value(lux_m2.c_str());
     std::string lux_m3 = to_string_with_precision(fuzzyParams.lux_m.at(3), 6);
     D->value(lux_m3.c_str());



     node_fuzzy_IO_set = node_fuzzy_IO->first_node("H"); // lux__h
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string lux_h0 = to_string_with_precision(fuzzyParams.lux_h.at(0), 6);
     A->value(lux_h0.c_str());
     std::string lux_h1 = to_string_with_precision(fuzzyParams.lux_h.at(1), 6);
     B->value(lux_h1.c_str());
     std::string lux_h2 = to_string_with_precision(fuzzyParams.lux_h.at(2), 6);
     C->value(lux_h2.c_str());
     std::string lux_h3 = to_string_with_precision(fuzzyParams.lux_h.at(3), 6);
     D->value(lux_h3.c_str());


     node_fuzzy_IO_set = node_fuzzy_IO->first_node("VH"); // lux__vh
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string lux_vh0 = to_string_with_precision(fuzzyParams.lux_vh.at(0), 6);
     A->value(lux_vh0.c_str());
     std::string lux_vh1 = to_string_with_precision(fuzzyParams.lux_vh.at(1), 6);
     B->value(lux_vh1.c_str());
     std::string lux_vh2 = to_string_with_precision(fuzzyParams.lux_vh.at(2), 6);
     C->value(lux_vh2.c_str());
     std::string lux_vh3 = to_string_with_precision(fuzzyParams.lux_vh.at(3), 6);
     D->value(lux_vh3.c_str());

//     //// ///___///___///___///___///___///___///___///___///___///___///___///___
     node_fuzzy_IO = node_fuzzy->first_node("exposure");  // exposure
     node_fuzzy_IO_set = node_fuzzy_IO->first_node("VL"); // exp_vl
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string exp_vl0 = to_string_with_precision(fuzzyParams.exp_vl.at(0), 6);
     A->value(exp_vl0.c_str());
     std::string exp_vl1 = to_string_with_precision(fuzzyParams.exp_vl.at(1), 6);
     B->value(exp_vl1.c_str());
     std::string exp_vl2 = to_string_with_precision(fuzzyParams.exp_vl.at(2), 6);
     C->value(exp_vl2.c_str());
     std::string exp_vl3 = to_string_with_precision(fuzzyParams.exp_vl.at(3), 6);
     D->value(exp_vl3.c_str());




     node_fuzzy_IO_set = node_fuzzy_IO->first_node("L"); // exp_l
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string exp_l0 = to_string_with_precision(fuzzyParams.exp_l.at(0), 6);
     A->value(exp_l0.c_str());
     std::string exp_l1 = to_string_with_precision(fuzzyParams.exp_l.at(1), 6);
     B->value(exp_l1.c_str());
     std::string exp_l2 = to_string_with_precision(fuzzyParams.exp_l.at(2), 6);
     C->value(exp_l2.c_str());
     std::string exp_l3 = to_string_with_precision(fuzzyParams.exp_l.at(3), 6);
     D->value(exp_l3.c_str());



     node_fuzzy_IO_set = node_fuzzy_IO->first_node("M"); // exp_m
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string exp_m0 = to_string_with_precision(fuzzyParams.exp_m.at(0), 6);
     A->value(exp_m0.c_str());
     std::string exp_m1 = to_string_with_precision(fuzzyParams.exp_m.at(1), 6);
     B->value(exp_m1.c_str());
     std::string exp_m2 = to_string_with_precision(fuzzyParams.exp_m.at(2), 6);
     C->value(exp_m2.c_str());
     std::string exp_m3 = to_string_with_precision(fuzzyParams.exp_m.at(3), 6);
     D->value(exp_m3.c_str());



     node_fuzzy_IO_set = node_fuzzy_IO->first_node("H"); // exp_h
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string exp_h0 = to_string_with_precision(fuzzyParams.exp_h.at(0), 6);
     A->value(exp_h0.c_str());
     std::string exp_h1 = to_string_with_precision(fuzzyParams.exp_h.at(1), 6);
     B->value(exp_h1.c_str());
     std::string exp_h2 = to_string_with_precision(fuzzyParams.exp_h.at(2), 6);
     C->value(exp_h2.c_str());
     std::string exp_h3 = to_string_with_precision(fuzzyParams.exp_h.at(3), 6);
     D->value(exp_h3.c_str());


     node_fuzzy_IO_set = node_fuzzy_IO->first_node("VH"); // exp_h
     A = node_fuzzy_IO_set->first_node();
     B = A->next_sibling();
     C = B->next_sibling();
     D = C->next_sibling();

     std::string exp_vh0 = to_string_with_precision(fuzzyParams.exp_vh.at(0), 6);
     A->value(exp_vh0.c_str());
     std::string exp_vh1 = to_string_with_precision(fuzzyParams.exp_vh.at(1), 6);
     B->value(exp_vh1.c_str());
     std::string exp_vh2 = to_string_with_precision(fuzzyParams.exp_vh.at(2), 6);
     C->value(exp_vh2.c_str());
     std::string exp_vh3 = to_string_with_precision(fuzzyParams.exp_vh.at(3), 6);
     D->value(exp_vh3.c_str());




    std::ofstream file_stored("../data/config.xml");
    file_stored  << doc;
    file_stored.close();
}
