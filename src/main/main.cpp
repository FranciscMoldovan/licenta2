#include <cv.h>
#include <opencv/highgui.h>
#include <raspicam/raspicam.h>
#include <raspicam/raspicam_cv.h>
#include <iostream>
#include <time.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "queuefixed.h"
#include "pidcontroller.h"
#include "luminositysensor.h"
#define SIZE 20

#include <FuzzyRule.h>
#include <FuzzyComposition.h>
#include <Fuzzy.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>
#include <FuzzyIO.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>

#define BRIGHTNESS_MIN 0
#define BRIGHTNESS_MAX 100

// value of exposure in us
#define EXPOSURE_MIN 1
#define EXPOSURE_MAX 100000

// GUI
#include "../build/ui_mainwindow.h"
#include "../include/ui/mainwindow.h"
#include <QApplication>

#include "main.h"
#include "exp_algs.h"
#include <read_xml.h>

#include <sys/time.h>


#include <omp.h>

void setFuzzyFields(MainWindow &w);
enum variation
{
  NOT, ASC, DESC
};

enum block_state
{
  POT_BRIGHT, POT_DARK, BRIGHT, DARK, DK
};

typedef struct
{
  int brightness;
  int x;
  int y;
  block_state block_state_x = DK;
  block_state block_state_y = DK;
} BlockT;

variation cmpBrightnesses(int &brightness1, int &brightness2, int threshold);
int main(int argc, char **argv)
{

  // get the PID and Fuzzy parameters from the XML doc
  read_xml_params();

  // Initialize the Fuzzy Controller
  fuzzy_init();

  // UI in the two lines below
  QApplication a(argc, argv);
  MainWindow w;

  // Filling the PID parameter fields with data
  w.GetUi()->kpField->setText(QString::number(pid.getP()));
  w.GetUi()->kiField->setText(QString::number(pid.getI()));
  w.GetUi()->kdField->setText(QString::number(pid.getD()));
  w.GetUi()->samplingTimeField->setText(QString::number(pid.getDT()));

  // Filling the Fuzzy parameter fields with data
  setFuzzyFields(w);



  // Set Width and Height of images retrieved from camera
  Camera.set(CV_CAP_PROP_FRAME_HEIGHT, 320);
  Camera.set(CV_CAP_PROP_FRAME_WIDTH, 640);

  // Set color/BW camera output (CV_8fUC3 for color and CV_8UC1 for gray levels)
  Camera.set(CV_CAP_PROP_FORMAT, CV_8UC3);

  // Open camera
  std::cout << "Opening camera..." << std::endl;
  if(!Camera.open())
  {
    std::cerr << "Error opening camera!" << std::endl;
    return -1;
  }
  std::cout << "OPEN~!!\n";

  // set a default first 1000 us
  Camera.set(CV_CAP_PROP_EXPOSURE, 1000);

  // The integer which shows the current tabbed element chose
  int change;

  // List with grid sizes
  QStringList list = (QStringList() << "5" << "10" << "15" << "20" << "25" << "30" << "12");
  w.GetUi()->comboBox->addItems(list);
  w.GetUi()->comboBox->setItemData(5, QColor(Qt::red), Qt::BackgroundColorRole);
  w.GetUi()->comboBox->setItemData(6, QColor(Qt::red), Qt::BackgroundColorRole);


  // Parameters for enhancement
  double avg_br_dark = 0;
  double frac_bright = 0;
  double  frac_dark = 0;
  double avg_br_bright = 0;

  int pot_dark_count = 0, pot_bright_count = 0, dark_count = 0, bright_count = 0;
  int sum_bright_count = 0;
  int sum_dark_count = 0;
  int target_brightness = 0;
  int remeberered_brightness;

  // The infinite loop of the system
  for(;;)
  {
    // Refresh or not the measured and calculated data for enhancement
    if(!w.GetUi()->checkBox_2->isChecked())
    {
      remeberered_brightness = brightness;
      w.GetUi()->lbl_lower_br->setText("---");
      w.GetUi()->lbl_upper_br->setText("---");
      w.GetUi()->lbl_frac->setText("---");
      pot_dark_count = pot_bright_count = dark_count = bright_count = 0;
      sum_bright_count = sum_dark_count = 0;
    }

    // Set the fuzzy controller parameter fields on a loop
    setFuzzyFields(w);

    // Check for update necessity for PID object
    if(w.needPIDUpdate())
    {
      PIDParams localPIDParams = w.getPIDParameters();
      pid.setP(localPIDParams.KP);
      pid.setI(localPIDParams.KI);
      pid.setD(localPIDParams.KD);
      pid.setDT(localPIDParams.SamplingTime);

      persist_data(localPIDParams);
    }

    // Check for update necessity for Fuzzy Controller
    if(w.needFuzzyUpdate())
    {
      FuzzyParams localFuzzyParams = w.getFuzzyParameters();
      br__VL = new FuzzySet(localFuzzyParams.br_vl.at(0),
                            localFuzzyParams.br_vl.at(1),
                            localFuzzyParams.br_vl.at(2),
                            localFuzzyParams.br_vl.at(3));

      br__L = new FuzzySet(localFuzzyParams.br_l.at(0),
                           localFuzzyParams.br_l.at(1),
                           localFuzzyParams.br_l.at(2),
                           localFuzzyParams.br_l.at(3));

      br__M = new FuzzySet(localFuzzyParams.br_m.at(0),
                           localFuzzyParams.br_m.at(1),
                           localFuzzyParams.br_m.at(2),
                           localFuzzyParams.br_m.at(3));

      br__H = new FuzzySet(localFuzzyParams.br_h.at(0),
                           localFuzzyParams.br_h.at(1),
                           localFuzzyParams.br_h.at(2),
                           localFuzzyParams.br_h.at(3));

      br__VH = new FuzzySet(localFuzzyParams.br_vh.at(0),
                            localFuzzyParams.br_vh.at(1),
                            localFuzzyParams.br_vh.at(2),
                            localFuzzyParams.br_vh.at(3));


      exposure__VL = new FuzzySet(localFuzzyParams.exp_vl.at(0),
                                  localFuzzyParams.exp_vl.at(1),
                                  localFuzzyParams.exp_vl.at(2),
                                  localFuzzyParams.exp_vl.at(3));

      exposure__L = new FuzzySet(localFuzzyParams.exp_l.at(0),
                                 localFuzzyParams.exp_l.at(1),
                                 localFuzzyParams.exp_l.at(2),
                                 localFuzzyParams.exp_l.at(3));

      exposure__M = new FuzzySet(localFuzzyParams.exp_m.at(0),
                                 localFuzzyParams.exp_m.at(1),
                                 localFuzzyParams.exp_m.at(2),
                                 localFuzzyParams.exp_m.at(3));

      exposure__H = new FuzzySet(localFuzzyParams.exp_h.at(0),
                                 localFuzzyParams.exp_h.at(1),
                                 localFuzzyParams.exp_h.at(2),
                                 localFuzzyParams.exp_h.at(3));

      exposure__VH = new FuzzySet(localFuzzyParams.exp_vh.at(0),
                                  localFuzzyParams.exp_vh.at(1),
                                  localFuzzyParams.exp_vh.at(2),
                                  localFuzzyParams.exp_vh.at(3));


      light__VL = new FuzzySet(localFuzzyParams.lux_vl.at(0),
                               localFuzzyParams.lux_vl.at(1),
                               localFuzzyParams.lux_vl.at(2),
                               localFuzzyParams.lux_vl.at(3));

      light__L = new FuzzySet(localFuzzyParams.lux_l.at(0),
                              localFuzzyParams.lux_l.at(1),
                              localFuzzyParams.lux_l.at(2),
                              localFuzzyParams.lux_l.at(3));

      light__M = new FuzzySet(localFuzzyParams.lux_m.at(0),
                              localFuzzyParams.lux_m.at(1),
                              localFuzzyParams.lux_m.at(2),
                              localFuzzyParams.lux_m.at(3));

      light__H = new FuzzySet(localFuzzyParams.lux_h.at(0),
                              localFuzzyParams.lux_h.at(1),
                              localFuzzyParams.lux_h.at(2),
                              localFuzzyParams.lux_h.at(3));

      light__VH = new FuzzySet(localFuzzyParams.lux_vh.at(0),
                               localFuzzyParams.lux_vh.at(1),
                               localFuzzyParams.lux_vh.at(2),
                               localFuzzyParams.lux_vh.at(3));



      setFuzzyFields(w);


      persist_data(localFuzzyParams);
      //      w.GetUi()->btnSetFuzzy->setText("TEST");
    }

    // Get the measurement from TSL2561 light sensor
    lux_measured = LightSensor.MeasureLuminosity();

    // Diplay light intensity measurement on GUI
    w.GetUi()->sensor_reading_label->setText(QString::number(lux_measured) + " LUX");

    // Display ROI brightness on GUI
    w.GetUi()->brightness_label->setText("BR: " + QString::number(brightness));

    // Grab a frame from the camera
    Camera.grab();

    // store the frame in a cv::Mat object
    Camera.retrieve(frame);

    // calculate the ROI brightness based on the Green channel alone
    brightness = getBrightness(roiImg);

    // Get a rectangle showing the ROI top-left corner and dimensions
    rect = cv::Rect(w.roiParams.x_rect,    w.roiParams.y_rect,
                    w.roiParams.width_rect, w.roiParams.height_rect);

    // Rows and cols for the brightness matrix
    int rows = 0;
    int cols = 0;

    // Get the GRID size from GUI
    int GRID_SIZE = w.GetUi()->comboBox->itemText(w.GetUi()->comboBox->currentIndex()).toInt();


    // green squares with red filling!
    // Parallel image analyzer. Only works for grids different from 12 and 30 in size
    if(GRID_SIZE != 30 && GRID_SIZE != 12)
    {
      if(w.GetUi()->checkBox->isChecked())
      {
        omp_set_num_threads(4);

        // Update rows
        rows = (w.roiParams.height_rect - 5) / GRID_SIZE;

        // Update cols
        cols = (w.roiParams.width_rect - 5) / GRID_SIZE;

        std::vector<int> brightnesses(rows * cols); // = new std::vector<int>();

        int sum = 0;
        int sums[rows * cols] = {0};


        //        for(int i = w.roiParams.x_rect, j = 0; i < w.roiParams.x_rect + w.roiParams.width_rect - GRID_SIZE, j < rows; i += GRID_SIZE, j++)
        //        {
        //          std::cout << i << ", " << j << std::endl;
        //        }


        double test_time_start = omp_get_wtime();
        #pragma omp parallel
        {
          #pragma omp for schedule(dynamic, 1)
          // for(int i = w.roiParams.x_rect; i < w.roiParams.x_rect + w.roiParams.width_rect - GRID_SIZE; i += GRID_SIZE)
          for(int i = 0; i < cols; i++)
          {
            //            ii=0;
            //for(int j = w.roiParams.y_rect; j < w.roiParams.y_rect + w.roiParams.height_rect - GRID_SIZE; j += GRID_SIZE)
            for(int j = 0; j < rows; j++)
            {
              cv::Rect x(w.roiParams.x_rect + i * GRID_SIZE, w.roiParams.y_rect + j * GRID_SIZE, GRID_SIZE, GRID_SIZE);
              cv::rectangle(frame, cv::Point(w.roiParams.x_rect + i * GRID_SIZE, w.roiParams.y_rect + j * GRID_SIZE), cv::Point(w.roiParams.x_rect + i * GRID_SIZE + GRID_SIZE, w.roiParams.y_rect + j * GRID_SIZE + GRID_SIZE), cv::Scalar(0, 255, 0), 1, 1, 0);

              int aBrgh = getBrightness(frame(x));


              if(aBrgh > 65)
              {
                cv::Mat roi = frame(x);
                cv::Mat color(roi.size(), CV_8UC3, cv::Scalar(0, 0, 255));
                double alpha = 0.6;
                cv::addWeighted(color, alpha, roi, 1.0 - alpha, 0.0, roi);
              }

            }
          }
        }
        double test_time_end = omp_get_wtime();
        //        delete bright_vect;
        brightnesses.clear();
        //        std::cout << "TIME SPENT WITH PARALLELISM: " << 1000 * (test_time_end - test_time_start) << "ms" << std::endl;

        //                for (int i = 0; i < rows; ++i) {
        //                    for (int j = 0; j < cols; ++j) {
        //                    std::cout << brightness_mat[i][j] <<  "   ";
        //                    }
        //                    std::cout << "\n";
        //                }

        //        std::cout << "S.U.M.=" << sum << std::endl;
        double end_parallel = omp_get_wtime();
        //        std::cout << "TIME FOR GRIDS===" << end_parallel - start_parallel;

      }
    }
    else // apply main obj det algorithm
    {
      int cnt_ext = 0, cnt_int = 0;
      int wth = (w.roiParams.width_rect - 5) / GRID_SIZE;
      int hgt = (w.roiParams.height_rect - 5) / GRID_SIZE;
      variation vars[wth][hgt];
      std::string brightness_mat[wth][hgt];

      printf("\n\nWTH=%d, HGT=%d\n\n", wth, hgt);
      int number = 0;

      std::vector<int> brightness_vect;
      std::vector<BlockT> blocks_vect;
      std::vector<cv::Rect> rect_vect;




      for(int i = w.roiParams.y_rect; i < w.roiParams.y_rect + w.roiParams.height_rect - GRID_SIZE; i += GRID_SIZE)
      {
        cnt_int = 0;
        for(int j = w.roiParams.x_rect; j < w.roiParams.x_rect + w.roiParams.width_rect - GRID_SIZE; j += GRID_SIZE)
        {
          cv::Rect x(j, i, GRID_SIZE, GRID_SIZE);

          rect_vect.push_back(x);

          cv::rectangle(frame, cv::Point(j, i), cv::Point(j + GRID_SIZE, i + GRID_SIZE), cv::Scalar(255, 0, 0), 1, 1, 0);
          int aBrgh = getBrightness(frame(x));
          //          cv::putText(frame, std::to_string(number++), cv::Point(x.x, x.y+GRID_SIZE), cv::FONT_HERSHEY_COMPLEX, 0.6,
          //                      cv::Scalar(0,255,0), 1, CV_AA);


          cv::putText(frame, std::to_string(cnt_ext) + "|" + std::to_string(cnt_int), cv::Point(x.x, x.y + GRID_SIZE - 5), cv::FONT_HERSHEY_COMPLEX, 0.3,
                      cv::Scalar(0, 0, 0), 1, CV_AA);

          cv::putText(frame, std::to_string(aBrgh), cv::Point(x.x, x.y + GRID_SIZE / 2), cv::FONT_HERSHEY_COMPLEX, 0.45,
                      cv::Scalar(255, 255, 0), 1, CV_AA);

          //          brightness_vect.push_back(std::to_string(cnt_ext) + "|" + std::to_string(cnt_int));

          if(aBrgh == 0)
          {
            aBrgh = 1; // too avoid FP error
          }

          //          brightness_vect.push_back(aBrgh);

          BlockT dummy_block;

          dummy_block.x = x.x;
          dummy_block.y = x.y;
          dummy_block.brightness = aBrgh;

          blocks_vect.push_back(dummy_block);

          cnt_int++;
        }
        //        std::cout << " | ";
        cnt_ext++;
      }


      //      std::cout << "\n";

      //      foreach(int var, brightness_vect)
      //      {
      //        std::cout << var << ", ";
      //      }



      ////////////////////// USELESS ALGO START //////////////////////
      //      // the algorithm
      //      int jj = 0, ii = 0;

      //      double start_time1 = omp_get_wtime();

      ////#pragma omp parallel for
      //      for(ii = 0; ii < hgt; ii++)
      //      {
      //        for(jj = 0; jj < wth; jj++)
      //        {
      //          variation var = cmpBrightnesses(brightness_vect[ii * wth + jj], brightness_vect[ii * wth + (jj + 1)], 15);
      //          switch(var)
      //          {
      //          case NOT:
      //            {
      //              std::cout << "NOT, ";
      //              cv::rectangle(frame, rect_vect[ii * wth + jj], cv::Scalar(0, 0, 0), 2.5, 8, 0);
      //            }
      //            break;

      //          case ASC:
      //            {
      //              std::cout << "ASC, ";
      //              cv::rectangle(frame, rect_vect[ii * wth + jj], cv::Scalar(0, 255, 0), 2.5, 8, 0);
      //            }
      //            break;

      //          case DESC:
      //            {
      //              cv::rectangle(frame, rect_vect[ii * wth + jj], cv::Scalar(0, 0, 255), 2.5, 8, 0);
      //              std::cout << "DESC, ";
      //            }
      //            break;

      //          default:
      //            break;
      //          }

      //        }

      //      }

      /////////////////// USELESS ALGO END /////////////////

      //      std::cout << "\n\nNUMBER OF BLOCKS COUNTED=" << blocks_vect.size() << std::endl;

      std::cout << "\n================================\n";

      frac_dark = frac_bright = 0;

      ////////////////////////////////////////////////////////
      //////// INSERT PAPER-INSPIRED ALGO HERE  /////////////
      ////////////////////////////////////////////////////////
      int i = 0, j = 0;
      //      omp_set_num_threads(4);
      double start_alg = omp_get_wtime();
      //#pragma omp parallel for


      int th_horiz = 50;
      //////////////////////////////////////////////////////
      //////////////////////////////////////////////////////
      // HORIZONTAL SEARCH
      //////////////////////////////////////////////////////
      //////////////////////////////////////////////////////
      for(i = 0; i < hgt; i++)
      {
        //          std::cout << "FOR#" << i << std::endl;
        j = 0;
        do
        {
          //          std::cout << "analyzing " << "[" << i << "]" << "[" << j << "]" << std::endl;
          // compare with the x axes neighbour

          //          std::cout << "[" << i << "]" << "[" << j << "] vs [" << i << "]" << "[" << j + 1 << "]" ;
          //          std::cout << std::endl << blocks_vect[i * wth + j].brightness << " vs " << blocks_vect[i * wth + j + 1].brightness;
          //          std::cout << "COMPARISON RESULT" << cmpBrightnesses(blocks_vect[i * wth + j].brightness, blocks_vect[i * wth + j + 1].brightness, 70);
          if(cmpBrightnesses(blocks_vect[i * wth + j].brightness, blocks_vect[i * wth + j + 1].brightness, th_horiz) == ASC)
          {
            //            std::cout << "\n[1]\n";
            // 1. ASC
            if(cmpBrightnesses(blocks_vect[i * wth + j + 1].brightness, blocks_vect[i * wth + j + 2].brightness, th_horiz) == ASC)
            {
              //              std::cout << "\n[1.1]\n";
              // Do nothing
              //              std::cout << "j INCREASED\n\n";
              j++;
            }
            // 2. DESC
            else if(cmpBrightnesses(blocks_vect[i * wth + j + 1].brightness, blocks_vect[i * wth + j + 2].brightness, th_horiz) == DESC)
            {
              //              std::cout << "\n[1.2]\n";
              // Mark the block potentially bright in that direction
              blocks_vect[i * wth + j].block_state_x = POT_BRIGHT;
              //              std::cout << "j INCREASED\n\n";

              pot_bright_count++;

              j++;
            }
            // 3.NOT
            else if(cmpBrightnesses(blocks_vect[i * wth + j + 1].brightness, blocks_vect[i * wth + j + 2].brightness, th_horiz) == NOT)
            {
              if(blocks_vect[i * wth + j + 1].brightness / blocks_vect[i * wth + j].brightness > 2)
              {
                //              std::cout << "\n[1.3]\n";
                // Turn on the bright flag; Store the position of B(i,j)
                //                    std::cout << "RAPORT=" << (float)blocks_vect[i * wth + j + 2].brightness / blocks_vect[i * wth + j + 1].brightness;
                std::cout << "\n@@@@@@@@@ HORIZ BRIGHT @@@@@@@@ FOUND (" << i << ", " << j << ")" << std::endl;

                cv::putText(frame, std::to_string(i) + "||" + std::to_string(j), cv::Point(blocks_vect[i * wth + j].x,
                            blocks_vect[i * wth + j].y + GRID_SIZE - 5), cv::FONT_HERSHEY_COMPLEX, 0.3,
                            cv::Scalar(0, 255, 0), 1, CV_AA);

                blocks_vect[i * wth + j].block_state_x = BRIGHT;
                //              std::cout << "j INCREASED\n\n";
                if(!w.GetUi()->checkBox_2->isChecked())
                {
                  bright_count++;
                  sum_bright_count += blocks_vect[i * wth + j + 1].brightness;
                }
              }
              j++;
            }
          }
          else if(cmpBrightnesses(blocks_vect[i * wth + j].brightness, blocks_vect[i * wth + j + 1].brightness, th_horiz) == DESC)
          {
            //            std::cout << "\n[2]\n";
            // 1. ASC
            if(cmpBrightnesses(blocks_vect[i * wth + j + 1].brightness, blocks_vect[i * wth + j + 2].brightness, th_horiz) == ASC)
            {
              //              std::cout << "\n[2.1]\n";
              // Mark B(i,j) as potentially dark block in that direction
              blocks_vect[i * wth + j].block_state_x = POT_DARK;
              //              std::cout << "j INCREASED\n\n";
              pot_dark_count++;
              j++;
            }
            // 2. DESC
            else if(cmpBrightnesses(blocks_vect[i * wth + j + 1].brightness, blocks_vect[i * wth + j + 2].brightness, th_horiz) == DESC)
            {
              //              std::cout << "\n[2.2]\n";
              // Do nothing
              //              std::cout << "j INCREASED\n\n";
              j++;
            }
            // 3.NOT
            else if(cmpBrightnesses(blocks_vect[i * wth + j + 1].brightness, blocks_vect[i * wth + j + 2].brightness, th_horiz) == NOT)
            {
              //              std::cout << "\n[2.3]\n";
              // Turn on the dark flag; Store the position of B(i,j)
              //              std::cout << "            @@@@@@@@@ DARK @@@@@@@@ FOUND (" << i << ", " << j << ")" << std::endl;

              cv::putText(frame, std::to_string(i) + "||" + std::to_string(j), cv::Point(blocks_vect[i * wth + j].x,
                          blocks_vect[i * wth + j].y + GRID_SIZE - 5), cv::FONT_HERSHEY_COMPLEX, 0.3,
                          cv::Scalar(0, 0, 255), 1, CV_AA);

              blocks_vect[i * wth + j].block_state_x = DARK;
              //              std::cout << "j INCREASED\n\n";
              if(!w.GetUi()->checkBox_2->isChecked())
              {
                dark_count++;
                sum_dark_count += blocks_vect[i * wth + j + 1].brightness;
              }
              j++;
            }
          }
          else if(cmpBrightnesses(blocks_vect[i * wth + j].brightness, blocks_vect[i * wth + j + 1].brightness, th_horiz) == NOT)
          {
            //            std::cout << "\n[3]\n";
            if(cmpBrightnesses(blocks_vect[i * wth + j + 1].brightness, blocks_vect[i * wth + j + 2].brightness, th_horiz) == ASC)
            {
              //              std::cout << "\n[3.1]\n";
              if(blocks_vect[i * wth + j].block_state_x == DARK)
              {
                // = dark blocks
                blocks_vect[i * wth + j].block_state_x = POT_DARK;
                blocks_vect[i * wth + j + 1].block_state_x = POT_DARK;
                //                std::cout << "j INCREASED\n\n";
                pot_dark_count++;
                j++;
              }
              else
              {
                // dark flag off, do nothing
                //                std::cout << "j INCREASED\n\n";
                j++;
              }
            }
            else if(cmpBrightnesses(blocks_vect[i * wth + j + 1].brightness, blocks_vect[i * wth + j + 2].brightness, th_horiz) == DESC)
            {
              //              std::cout << "\n[3.2]\n";
              if(blocks_vect[i * wth + j].block_state_x == BRIGHT)
              {
                // Mark the potential bright blocks
                blocks_vect[i * wth + j].block_state_x = POT_BRIGHT;
                blocks_vect[i * wth + j + 1].block_state_x = POT_BRIGHT;
                //                std::cout << "j INCREASED\n\n";
                pot_bright_count += 2;
                j++;
              }
              else
              {
                // dark flag off, do nothing
                //                std::cout << "j INCREASED\n\n";
                j++;
              }
            }
            else if(cmpBrightnesses(blocks_vect[i * wth + j + 1].brightness, blocks_vect[i * wth + j + 2].brightness, th_horiz) == NOT)
            {
              //              std::cout << "\n[3.3]\n";
              // Do nothing.
              //              std::cout << "j INCREASED\n\n";
              j++;
            }

          }

        }
        while(j < wth - 2);
      }
      //////////////////////////////////////////////////////
      //////////////////////////////////////////////////////
      // END OF HORIZONTAL SEARCH
      //////////////////////////////////////////////////////
      //////////////////////////////////////////////////////


      int th_vert = 50;
      //////////////////////////////////////////////////////
      //////////////////////////////////////////////////////
      // VERTICAL SEARCH
      //////////////////////////////////////////////////////
      //////////////////////////////////////////////////////
      for(j = 0; j < wth; j++)
      {
        //          std::cout << "FOR#" << i << std::endl;
        i = 0;
        do
        {
          //          std::cout << "analyzing " << "[" << i << "]" << "[" << j << "]" << std::endl;
          // compare with the x axes neighbour

          //          std::cout << "[" << i << "]" << "[" << j << "] vs [" << i << "]" << "[" << j + 1 << "]" ;
          //          std::cout << std::endl << blocks_vect[i * wth + j].brightness << " vs " << blocks_vect[i * wth + j + 1].brightness;
          //          std::cout << "COMPARISON RESULT" << cmpBrightnesses(blocks_vect[i * wth + j].brightness, blocks_vect[i * wth + j + 1].brightness, 70);
          if(cmpBrightnesses(blocks_vect[i * wth + j].brightness, blocks_vect[(i + 1) * wth + j].brightness, th_vert) == ASC)
          {
            //            std::cout << "\n[1]\n";
            // 1. ASC
            if(cmpBrightnesses(blocks_vect[(i + 1)* wth + j].brightness, blocks_vect[(i + 2) * wth + j].brightness, th_vert) == ASC)
            {
              // Do nothing
              i++;
            }
            // 2. DESC
            else if(cmpBrightnesses(blocks_vect[(i + 1) * wth + j].brightness, blocks_vect[(i + 2)* wth + j].brightness, th_vert) == DESC)
            {
              //              std::cout << "\n[1.2]\n";
              // Mark the block potentially bright in that direction
              blocks_vect[i * wth + j].block_state_y = POT_BRIGHT;
              //              std::cout << "j INCREASED\n\n";
              pot_bright_count++;
              i++;
            }
            // 3.NOT
            else if(cmpBrightnesses(blocks_vect[(i + 1) * wth + j].brightness, blocks_vect[(i + 2) * wth + j].brightness, th_vert) == NOT)
            {
              // Turn on the bright flag; Store the position of B(i,j)
              //              std::cout << "@@@@@@@@@ BRIGHT @@@@@@@@ FOUND (" << i << ", " << j << ")" << std::endl;

              cv::putText(frame, std::to_string(i) + "||" + std::to_string(j), cv::Point(blocks_vect[i * wth + j].x,
                          blocks_vect[i * wth + j].y + GRID_SIZE - 5), cv::FONT_HERSHEY_COMPLEX, 0.3,
                          cv::Scalar(0, 255, 0), 1, CV_AA);

              blocks_vect[i * wth + j].block_state_y = BRIGHT;
              if(!w.GetUi()->checkBox_2->isChecked())
              {
                bright_count++;
                sum_bright_count += blocks_vect[(i + 1) * wth + j].brightness;
              }
              i++;
            }
          }
          else if(cmpBrightnesses(blocks_vect[i * wth + j].brightness, blocks_vect[(i + 1) * wth + j].brightness, th_vert) == DESC)
          {
            // 1. ASC
            if(cmpBrightnesses(blocks_vect[(i + 1)* wth + j].brightness, blocks_vect[(i + 2) * wth + j].brightness, th_vert) == ASC)
            {
              //              std::cout << "\n[2.1]\n";
              // Mark B(i,j) as potentially dark block in that direction
              blocks_vect[i * wth + j].block_state_y = POT_DARK;
              pot_dark_count++;
              i++;
            }
            // 2. DESC
            else if(cmpBrightnesses(blocks_vect[(i + 1)* wth + j].brightness, blocks_vect[(i + 2)* wth + j].brightness, th_vert) == DESC)
            {

              i++;
            }
            // 3.NOT
            else if(cmpBrightnesses(blocks_vect[(i + 1)* wth + j].brightness, blocks_vect[(i + 2)* wth + j].brightness, th_vert) == NOT)
            {
              // Turn on the dark flag; Store the position of B(i,j)
              //              std::cout << "            @@@@@@@@@ DARK @@@@@@@@ FOUND (" << i << ", " << j << ")" << std::endl;

              cv::putText(frame, std::to_string(i) + "||" + std::to_string(j), cv::Point(blocks_vect[i * wth + j].x,
                          blocks_vect[i * wth + j].y + GRID_SIZE - 5), cv::FONT_HERSHEY_COMPLEX, 0.3,
                          cv::Scalar(0, 0, 255), 1, CV_AA);

              blocks_vect[i * wth + j].block_state_y = DARK;
              if(!w.GetUi()->checkBox_2->isChecked())
              {
                dark_count++;
                sum_dark_count += blocks_vect[(i + 1) * wth + j].brightness;
              }
              i++;
            }
          }
          else if(cmpBrightnesses(blocks_vect[i * wth + j].brightness, blocks_vect[(i + 1) * wth + j].brightness, th_vert) == NOT)
          {
            if(cmpBrightnesses(blocks_vect[(i + 1)* wth + j].brightness, blocks_vect[(i + 2)* wth + j].brightness, th_vert) == ASC)
            {
              if(blocks_vect[i * wth + j].block_state_y == DARK)
              {
                // Mark the potential dark blocks
                blocks_vect[  i   * wth + j].block_state_y = POT_DARK;
                blocks_vect[(i + 1) * wth + j].block_state_y = POT_DARK;
                pot_dark_count += 2;
                i++;
              }
              else
              {
                // dark flag off, do nothing
                i++;
              }
            }
            else if(cmpBrightnesses(blocks_vect[(i + 1)* wth + j].brightness, blocks_vect[(i + 2)* wth + j].brightness, th_vert) == DESC)
            {
              if(blocks_vect[i * wth + j].block_state_x == BRIGHT)
              {
                // Mark the potential bright blocks
                blocks_vect[  i   * wth + j].block_state_y = POT_BRIGHT;
                blocks_vect[(i + 1) * wth + j].block_state_y = POT_BRIGHT;
                pot_bright_count += 2;
                i++;
              }
              else
              {
                // dark flag off, do nothing
                i++;
              }
            }
            else if(cmpBrightnesses(blocks_vect[(i + 1)* wth + j].brightness, blocks_vect[(i + 2)* wth + j].brightness, th_vert) == NOT)
            {
              // Do nothing.
              i++;
            }

          }

        }
        while(i < hgt - 2);
      }
      //////////////////////////////////////////////////////
      //////////////////////////////////////////////////////
      // END OF VERTICAL SEARCH
      //////////////////////////////////////////////////////
      //////////////////////////////////////////////////////



      std::cout << "DATA !!" <<  "\nPOT BRGH=" << pot_bright_count << "\nPOT DARK=" << pot_bright_count
                << "\nBRIGHT=" << bright_count << "\nDARK=" << dark_count << "\n";

      frac_bright = 100 * (double)bright_count / (bright_count + dark_count);
      frac_dark  = 100 * (double)dark_count / (bright_count + dark_count);

      w.GetUi()->progressBarBrightening->setValue(frac_bright);
      w.GetUi()->progressBarDarkening->setValue(frac_dark);




      std::cout << frac_bright << "% BRIGHT" << std::endl;
      std::cout << frac_dark << "% DARK" << std::endl;




      if(bright_count != 0)
      {
        std::cout << "[bright] DIVIDING" << sum_bright_count << " BY " << bright_count << std::endl;
        avg_br_bright = sum_bright_count / bright_count;
      }


      if(dark_count != 0)
      {
        std::cout << "[dark] DIVIDING" << sum_dark_count << " BY " << dark_count << std::endl;
        avg_br_dark = sum_dark_count / dark_count;
      }

      std::cout << "AVERAGE BRIGHT TILE BRIGHTNESS=" << avg_br_bright << std::endl;
      std::cout << "AVERAGE DARK TILE BRIGHTNESS=" << avg_br_dark << std::endl;

      double end_alg = omp_get_wtime();



      std::cout << (end_alg - start_alg) * 1000 << "ms\n";
      ////////////////////////////////////////////////////////
      //////// INSERT PAPER-INSPIRED ALGO HERE  /////////////
      ////////////////////////////////////////////////////////


      /////// END OF NONPARALLELIZED CODE (BLOCK_SIZE=30)
    }
    ///////////////////







    roiImg = frame(rect);



    // FOR NOW: calculate entire brightness



    change = w.currentTab();


    // chosing which algorithm to run
    if(change == 0)
    {
      auto_exposure2();
    }
    else if(change == 1)
    {

      //      if(frac_bright + frac_dark > 10)

      // dark fraction predominates => backlit => increase exposure
      if(w.GetUi()->checkBox_2->isChecked())
      {
        std::cout << "\n************@@@@@@@@@@2 IS CHECKED!!\n" ;
        std::cout << " ##### frac bright =" << frac_bright << " frac dark=" << frac_dark << "\n";
        if(frac_dark > frac_bright && frac_dark != 0 && frac_bright != 0)
        {
          std::cout << "***********************************MORE DAAAARK" << std::endl;
         float proportion =(float)frac_dark/frac_bright;
         w.GetUi()->lbl_frac->setText(QString::number(proportion));
         int br_addition;
         if (proportion > 1 && proportion <= 1.5)
         {
            br_addition = remeberered_brightness/3;
         }
         else if (proportion < 1.5 && proportion <= 2.5)
         {
             br_addition = remeberered_brightness/1.8;
         }
         else if (proportion >2.5 && proportion <= 2.8)
         {
             br_addition = remeberered_brightness/1.6;
         }
         else if (proportion > 2.8)
         {
             br_addition = remeberered_brightness/1.4;
         }
          target_brightness = remeberered_brightness+br_addition;
          std::cout << "\n\n !!!!!  " << target_brightness << "!!!!! \n\n";
          {
            w.GetUi()->lbl_upper_br->setText(QString::number(target_brightness));
            std::cout << "\nCOMPARING CURRENT=" << brightness << " with " << target_brightness << "\n";
            if(brightness < target_brightness)
            {
              Camera.set(CV_CAP_PROP_EXPOSURE, Camera.get(CV_CAP_PROP_EXPOSURE) + Camera.get(CV_CAP_PROP_EXPOSURE) / 6);
            }
          }
        }
        else if(frac_dark < frac_bright && frac_dark != 0 && frac_bright != 0)
        {
          std::cout << "************************************MORE BRIIIIIIGHT" << std::endl;
          target_brightness =  0.87 * avg_br_dark + 0.13 * avg_br_bright;
          std::cout << "\n\n !!!!!  " << target_brightness << "!!!!! \n\n";
          {
            w.GetUi()->lbl_lower_br->setText(QString::number(target_brightness));
            std::cout << "\nCOMPARING CURRENT=" << brightness << " with " << target_brightness << "\n";
            if(brightness > target_brightness)
            {
              Camera.set(CV_CAP_PROP_EXPOSURE, Camera.get(CV_CAP_PROP_EXPOSURE) - Camera.get(CV_CAP_PROP_EXPOSURE) / 6);
            }
          }

        }
      }
      else
      {
        auto_exposure3();
      }

    }
    else if(change == 2)
    {
      auto_exposure1();
    }


    // for debug purpose: display on image
    onImageParameters();

    // in remote desktop, Qt needs inversion of Red and Blue
    if(argv[1] == "REMOTE")
    {
      cv::cvtColor(frame, frame, CV_BGR2RGB);
    }

    // display image on label

    //    cv::Mat plane = cv::imread("picture.jpg", CV_LOAD_IMAGE_COLOR);

    //    cv::Mat dst;
    //    addWeighted(frame, 1.0, plane, 0.5, 0.0, dst);

    w.displayImageOnLabel(w.matToQImage(frame), *(w.GetUi())->label);


    w.GetUi()->progressBarExp->setValue(map(exposure, 0, 100000, 0, 100));
    w.GetUi()->progressBarLUX->setValue(map(lux_measured, 0, 10000, 0, 100));

    w.show();


    if(cv::waitKey(0) > 0)
    {
      break;
    }

  }
  return a.exec();
}

variation cmpBrightnesses(int &brightness1, int &brightness2, int threshold)
{
  variation myVar;
  float diff = 100 * (float)(brightness1 - brightness2) / (float)brightness1;
  //  float diff = (brightness1 - brightness2);
  //  std::cout << "\n" << brightness1 << " vs " << brightness2 << " diff was [" << diff << "%] => ";
  if(diff < 0)
  {
    // a higher brightness is met
    if(abs(diff) >= threshold)
    {
      // difference exceeds threshold
      //        std::cout << "ASC";
      myVar = ASC;
    }
    else
    {
      // not enough difference
      //        std::cout << "NOT";
      myVar = NOT;
    }
  }
  else if(diff > 0)
  {
    // smaller brightness encountered
    if(diff >= threshold)
    {
      // difference exceeds threshold
      //        std::cout << "DESC";
      myVar = DESC;
    }
    else
    {
      // not enough difference
      //        std::cout << "NOT";
      myVar = NOT;
    }
  }
  else
  {
    // they are equal
    //      std::cout << "NOT";
    myVar = NOT;
  }
  return myVar;
}


void setFuzzyFields(MainWindow &w)
{
  w.GetUi()->br_vl_1->setText(QString::number(br__VL->getPointA()));
  w.GetUi()->br_vl_2->setText(QString::number(br__VL->getPointB()));
  w.GetUi()->br_vl_3->setText(QString::number(br__VL->getPointC()));
  w.GetUi()->br_vl_4->setText(QString::number(br__VL->getPointD()));
  w.GetUi()->br_l_1->setText(QString::number(br__L->getPointA()));
  w.GetUi()->br_l_2->setText(QString::number(br__L->getPointB()));
  w.GetUi()->br_l_3->setText(QString::number(br__L->getPointC()));
  w.GetUi()->br_l_4->setText(QString::number(br__L->getPointD()));
  w.GetUi()->br_m_1->setText(QString::number(br__M->getPointA()));
  w.GetUi()->br_m_2->setText(QString::number(br__M->getPointB()));
  w.GetUi()->br_m_3->setText(QString::number(br__M->getPointC()));
  w.GetUi()->br_m_4->setText(QString::number(br__M->getPointD()));
  w.GetUi()->br_h_1->setText(QString::number(br__H->getPointA()));
  w.GetUi()->br_h_2->setText(QString::number(br__H->getPointB()));
  w.GetUi()->br_h_3->setText(QString::number(br__H->getPointC()));
  w.GetUi()->br_h_4->setText(QString::number(br__H->getPointD()));
  w.GetUi()->br_vh_1->setText(QString::number(br__VH->getPointA()));
  w.GetUi()->br_vh_2->setText(QString::number(br__VH->getPointB()));
  w.GetUi()->br_vh_3->setText(QString::number(br__VH->getPointC()));
  w.GetUi()->br_vh_4->setText(QString::number(br__VH->getPointD()));
  w.GetUi()->exp_vl_1->setText(QString::number(exposure__VL->getPointA()));
  w.GetUi()->exp_vl_2->setText(QString::number(exposure__VL->getPointB()));
  w.GetUi()->exp_vl_3->setText(QString::number(exposure__VL->getPointC()));
  w.GetUi()->exp_vl_4->setText(QString::number(exposure__VL->getPointD()));
  w.GetUi()->exp_l_1->setText(QString::number(exposure__L->getPointA()));
  w.GetUi()->exp_l_2->setText(QString::number(exposure__L->getPointB()));
  w.GetUi()->exp_l_3->setText(QString::number(exposure__L->getPointC()));
  w.GetUi()->exp_l_4->setText(QString::number(exposure__L->getPointD()));
  w.GetUi()->exp_m_1->setText(QString::number(exposure__M->getPointA()));
  w.GetUi()->exp_m_2->setText(QString::number(exposure__M->getPointB()));
  w.GetUi()->exp_m_3->setText(QString::number(exposure__M->getPointC()));
  w.GetUi()->exp_m_4->setText(QString::number(exposure__M->getPointD()));
  w.GetUi()->exp_h_1->setText(QString::number(exposure__H->getPointA()));
  w.GetUi()->exp_h_2->setText(QString::number(exposure__H->getPointB()));
  w.GetUi()->exp_h_3->setText(QString::number(exposure__H->getPointC()));
  w.GetUi()->exp_h_4->setText(QString::number(exposure__H->getPointD()));
  w.GetUi()->exp_vh_1->setText(QString::number(exposure__VH->getPointA()));
  w.GetUi()->exp_vh_2->setText(QString::number(exposure__VH->getPointB()));
  w.GetUi()->exp_vh_3->setText(QString::number(exposure__VH->getPointC()));
  w.GetUi()->exp_vh_4->setText(QString::number(exposure__VH->getPointD()));
  w.GetUi()->lux_vl_1->setText(QString::number(light__VL->getPointA()));
  w.GetUi()->lux_vl_2->setText(QString::number(light__VL->getPointB()));
  w.GetUi()->lux_vl_3->setText(QString::number(light__VL->getPointC()));
  w.GetUi()->lux_vl_4->setText(QString::number(light__VL->getPointD()));
  w.GetUi()->lux_l_1->setText(QString::number(light__L->getPointA()));
  w.GetUi()->lux_l_2->setText(QString::number(light__L->getPointB()));
  w.GetUi()->lux_l_3->setText(QString::number(light__L->getPointC()));
  w.GetUi()->lux_l_4->setText(QString::number(light__L->getPointD()));
  w.GetUi()->lux_m_1->setText(QString::number(light__M->getPointA()));
  w.GetUi()->lux_m_2->setText(QString::number(light__M->getPointB()));
  w.GetUi()->lux_m_3->setText(QString::number(light__M->getPointC()));
  w.GetUi()->lux_m_4->setText(QString::number(light__M->getPointD()));
  w.GetUi()->lux_h_1->setText(QString::number(light__H->getPointA()));
  w.GetUi()->lux_h_2->setText(QString::number(light__H->getPointB()));
  w.GetUi()->lux_h_3->setText(QString::number(light__H->getPointC()));
  w.GetUi()->lux_h_4->setText(QString::number(light__H->getPointD()));
  w.GetUi()->lux_vh_1->setText(QString::number(light__VH->getPointA()));
  w.GetUi()->lux_vh_2->setText(QString::number(light__VH->getPointB()));
  w.GetUi()->lux_vh_3->setText(QString::number(light__VH->getPointC()));
  w.GetUi()->lux_vh_4->setText(QString::number(light__VH->getPointD()));
}




