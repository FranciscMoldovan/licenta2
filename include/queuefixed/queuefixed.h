#ifndef QUEUEFIXED_H
#define QUEUEFIXED_H
#include <queue>
#include <list>
class QueueFixed
{
private:
        float sum;
        int size;
        std::vector<float>vals;
public:
    QueueFixed(int size);
    ~QueueFixed();
    int push(float val);
    int getSize();
    float getSum();
};

#endif // QUEUEFIXED_H
