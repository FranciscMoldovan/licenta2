#ifndef EXP_ALGS_H
#define EXP_ALGS_H

/**
 * @brief auto_exposure1
 *  increase-decrease, fixed-increment auto exposure algorithm
 */
void auto_exposure1();

/**
 * @brief auto_exposure2
 *  PID controller based auto exposure algorithm
 */
void auto_exposure2();

/**
 * @brief auto_exposure3
 *  fuzzy controller based auto exposure algorithm
 */
void auto_exposure3();


void auto_exposure4();



void fuzzy_init();
double map(double x, double in_min, double in_max, double out_min, double out_max);
#endif
