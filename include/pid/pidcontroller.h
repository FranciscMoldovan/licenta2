#ifndef _PID_H_
#define _PID_H_

#include <vector>

typedef struct
{
    double SamplingTime;
    double KP;
    double KI;
    double KD;
}PIDParams;

typedef struct
{
    int x_rect;
    int y_rect;
    int width_rect;
    int height_rect;
}RectProps;

typedef struct
{
    std::vector<double> br_vl;
    std::vector<double> br_l;
    std::vector<double> br_m;
    std::vector<double> br_h;
    std::vector<double> br_vh;

    std::vector<double> lux_vl;
    std::vector<double> lux_l;
    std::vector<double> lux_m;
    std::vector<double> lux_h;
    std::vector<double> lux_vh;

    std::vector<double> exp_vl;
    std::vector<double> exp_l;
    std::vector<double> exp_m;
    std::vector<double> exp_h;
    std::vector<double> exp_vh;
}FuzzyParams;


class PIDImpl;
class PID
{
    public:
        // Kp -  proportional gain
        // Ki -  Integral gain
        // Kd -  derivative gain
        // dt -  loop interval time
        // max - maximum value of manipulated variable
        // min - minimum value of manipulated variable
        PID();
        PID( double dt, double max, double min, double Kp, double Kd, double Ki );
        // Returns the manipulated variable given a setpoint and current process value
        double calculate( double setpoint, double pv );
        double getDT();
        double getP();
        double getI();
        double getD();

        void setP(double kp);
        void setI(double ki);
        void setD(double kd);

        void setDT(double dt);
        ~PID();

    private:
        PIDImpl *pimpl;
};

#endif
