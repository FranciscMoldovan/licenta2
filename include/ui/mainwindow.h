#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <QLabel>
#include <QLineEdit>
#include <QCloseEvent>
#include <QMessageBox>
#include <QTabWidget>
#include <../include/pid/pidcontroller.h>

#define SZ_ST 10

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  virtual ~MainWindow();
  QImage matToQImage(cv::Mat const &src);
  void displayImageOnLabel(const QImage &image, QLabel &label);
  Ui::MainWindow *GetUi();
  double getDoubleFromInput(const QLineEdit &textField);
  void closeEvent(QCloseEvent *event);
  void setFuzzyNeed(bool val);

  bool needPIDUpdate();
  bool needFuzzyUpdate();
  bool needTabUpdate();

  RectProps roiParams;

public slots:
  void setPIDParameters();
  void setFuzzyParameters();
  int moveROIUp();
  int moveROIDown();
  int moveROILeft();
  int moveROIRight();
  int increaseROIHeight();
  int increaseROIWidth();
  int decreaseROIHeight();
  int decreaseROIWidth();
  PIDParams getPIDParameters();
  FuzzyParams getFuzzyParameters();
  int currentTab();



signals:
  void pidChanged(PID newPID);

private:
  Ui::MainWindow *ui;
  bool need_pid_update;
  bool need_fuzzy_update;

  PIDParams localPIDParams;
  FuzzyParams localFuzzyParams;





};

#endif // MAINWINDOW_H




