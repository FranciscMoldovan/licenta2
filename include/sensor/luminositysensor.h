#ifndef LUMINOSITYSENSOR_H
#define LUMINOSITYSENSOR_H
#include "TSL2561.h"
class LSensorImpl;
class LuminositySensor
{
public:
    LuminositySensor();
    uint32_t MeasureLuminosity();
    ~LuminositySensor();
private:
    LSensorImpl *limpl;
};

#endif // LUMINOSITYSENSOR_H
