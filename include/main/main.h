#ifndef MAIN_H
#define MAIN_H


#include <raspicam/raspicam.h>
#include <raspicam/raspicam_cv.h>
#include <iostream>

#include "pidcontroller.h"
#include "luminositysensor.h"
#define SIZE 20

#include <FuzzyRule.h>
#include <FuzzyComposition.h>
#include <Fuzzy.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>
#include <FuzzyIO.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>

#define BRIGHTNESS_MIN 0
#define BRIGHTNESS_MAX 100

// value of exposure in us
#define EXPOSURE_MIN 1
#define EXPOSURE_MAX 100000

// GUI
//#include "../build/ui_mainwindow.h"
//#include "../include/ui/mainwindow.h"
//#include <QApplication>

  raspicam::RaspiCam_Cv Camera; // Camera Object
  PID pid;
  Fuzzy *fuzzy;
  cv::Mat roiImg;
  cv::Rect rect;
  cv::Scalar textColor(0, 0, 255);
  int brightness;
  LuminositySensor LightSensor;
  cv::Mat frame;
  double exposure;
  int lux_measured;

  FuzzySet *exposure__VL;
  FuzzySet *exposure__L ;
  FuzzySet *exposure__M ;
  FuzzySet *exposure__H ;
  FuzzySet *exposure__VH;

  FuzzySet *br__VL;
  FuzzySet *br__L ;
  FuzzySet *br__M ;
  FuzzySet *br__H ;
  FuzzySet *br__VH;

  FuzzySet *light__VL;
  FuzzySet *light__L ;
  FuzzySet *light__M ;
  FuzzySet *light__H ;
  FuzzySet *light__VH;

  void onImageParameters()
  {
    cv::rectangle(frame, rect, textColor, 1.5, 8, 0);
    cv::putText(frame, "EXP=" + std::to_string((int)Camera.get(CV_CAP_PROP_EXPOSURE)),
                cvPoint(0, 19), cv::FONT_HERSHEY_COMPLEX, 0.8,
                textColor, 1, CV_AA);

//    cv::putText(frame, "kp=" + std::to_string(pid.getP()) +
//                " ki=" + std::to_string(pid.getI()) + " kd=" + std::to_string(pid.getD()),
//                cvPoint(30, 55), cv::FONT_HERSHEY_COMPLEX, 0.8,
//                textColor, 1, CV_AA);
  }


  int getBrightness(const cv::Mat &frame)
  {
    cv::Mat temp, color[3], lum;
    temp = frame;
    cv::split(temp, color);

//    color[0] = color[0] * 0.299;
//    color[1] = color[1] * 0.587;
//    color[2] = color[2] * 0.114;

//    lum = color[0] + color[1] + color[2];

    lum = color[1];

    cv::Scalar summ = cv::sum(lum);

    int brightness = summ[0] / ((255) * frame.rows * frame.cols) * BRIGHTNESS_MAX;
    return brightness;
  }


#endif
